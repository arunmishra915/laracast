<?php

namespace App\Http\Middleware;

use Closure;

class VarifyUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if($request->session()->has('user')){
        $sessionUser = $request->session()->get('user');
        if(in_array($sessionUser->role,array(1,2))){
          return $next($request);
        }
        else{
          return redirect('dashboard');
        }
      }
    }
}
