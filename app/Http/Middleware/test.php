<?php

namespace App\Http\Middleware;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      //$ip = $request->ip();
      $ip = '523.236.25';
      if($ip == '127.0.0.1' || $ip == '::1'){
        return redirect('/');
      }
      return $next($request);
    }
}
