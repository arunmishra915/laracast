<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Holiday;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $pageHeading = "Holiday List";
      $year = date('Y');
      $normalHolidays = Holiday::where('type','=',1)->whereYear('date', $year)->orderBy('date','ASC')->get();
      $restrictedHolidays = Holiday::where('type','=',2)->whereYear('date', $year)->orderBy('date','ASC')->get();
      return view('holiday.index', compact('pageHeading','normalHolidays','restrictedHolidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
      $pageHeading = "Add Holiday";
      $holidays = Holiday::orderBy('date','ASC')->get();
      return view('holiday.create', compact('pageHeading','holidays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      $dates = $request->date;
      $holiday = $request->holiday;
      $type = $request->type;
      if(is_array($dates)){
        foreach($dates as $key=>$date){
          $insert = array();
          $insert['holiday'] = ucwords($holiday[$key]);
          $insert['date'] = $date;
          $insert['type'] = $type[$key];
          Holiday::create($insert);
        }
        return back()->with('success', 'Holiday has been added successfully.');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id){
       $holiday = Holiday::findOrFail($id);
       $holiday->delete();
       return back()->with('success', 'Holiday has been deleted successfully.');
     }
}
