<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Employee;
use App\Leave;
use App\Holiday;
use App\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\LeaveApplyMail;
use App\Mail\LeaveConfirmMail;
use App\Mail\LeaveCancelMail;
use Illuminate\Support\Facades\Session;
use View;
use DB;
class LeaveController extends Controller{
  public $paidLeave = 0;
  public $unpaidLeave = 0;
  public function __construct(){
    parent::__construct();

    //echo Route::getCurrentRoute()->getName();die;
  }
  public function index(){
    $pageHeading = "Applied Leaves";
    $employee = Employee::select(['leave_balance'])->where('id','=',$this->user->id)->first();
    $leaves = Leave::where('employee_id','=',$this->user->id)->get();
    return view('leave.index', compact('pageHeading','leaves','employee'));
  }

  public function create(){
    $pageHeading = "Apply Leave";
    $employee = Employee::select(['leave_balance'])->where('id','=',$this->user->id)->first();
    return view('leave.create', compact('pageHeading','employee'));
  }

  public function store(Request $request){
    $validate['from_date'] = 'required|date';
    $validate['to_date'] = 'required|date';
    $validate['reason'] = 'required';
    $request->validate($validate);
    if(empty($request->date)){
      return redirect('leave/create');
    }
    $insert['employee_id'] = $this->user->id;
    $insert['from_date'] = $request->from_date;
    $insert['to_date'] = $request->to_date;
    $insert['leave_status'] = '0';
    $leave_detail = array();
    $leave_count = 0;
    $employee = Employee::select(['name','leave_balance'])->where('id','=',$this->user->id)->first();
    $availabelLeaves = $employee->leave_balance;
    $duplicateLeaves = array();
    foreach($request->date as $key=>$date){
      $checkLeave = Leave::where('leave_detail', 'LIKE', "%$date%")->where('employee_id','=',$this->user->id)->get();
      if(count($checkLeave) > 0){
        $exist = getAlreadyApplyLeaveInfo($checkLeave, $date);
        if(trim($exist)!=""){
          array_push($duplicateLeaves,$exist);
        }
      }
      $sub = array();
      $sub['date'] = $date;
      $sub['leave_type'] = $request->leave_type[$key];
      if($availabelLeaves > $leave_count){
        if($request->leave_type[$key] == 'full'){
          $leave_count = $leave_count+1;
        }
        else{
          $leave_count = $leave_count+0.5;
        }
        $sub['is_paid_leave'] = 1;
      }
      else{
        $sub['is_paid_leave'] = 0;
      }
      $sub['half_period'] = (!empty($request->half_type[$key]))?$request->half_type[$key]:'';
      $sub['is_approved'] = 1;
      array_push($leave_detail, $sub);
    }
    if(count($duplicateLeaves) > 0){
      $leaveError = implode($duplicateLeaves,'<br>');
      flash($leaveError)->error();
      return redirect('leave/create');
    }
    $insert['leave_detail'] = json_encode($leave_detail);
    $insert['reason'] = $request->reason;
    $insert['leave_count'] = $leave_count;
    $insert['have_leave_balance'] = $availabelLeaves;
    $update['leave_balance'] = ($employee->leave_balance - $leave_count);
    $leave_id = Leave::create($insert)->id;
    Employee::whereId($this->user->id)->update($update);
    $notification['from_id'] = $this->user->id;
    $notification['to_id'] = 0;
    $notification['is_read'] = '0';
    $notification['notification_type'] = 'leave-apply';
    $notification['notification_url'] = route('leave.detail',$leave_id);
    $notification['message'] = $employee->name." has applied for leaves";
    Notification::create($notification);
    $data = array(
      'name' => $employee->name,
      'email' => $request->email,
      'from_date' => $request->from_date,
      'to_date' => $request->to_date,
      'reason' => $request->reason,
      'leave_detail' => $leave_detail
    );
    Mail::to(env('ADMIN_EMAIL'))->send(new LeaveApplyMail($data));
    flash('Leave has been applied successfully.')->success();
    return redirect('leave/create');
  }

  public function edit($id){
    $pageHeading = "Edit Leave";
    $leave = Leave::where('id','=',$id)->where('employee_id','=',$this->user->id)->first();
    if($leave){
      if(!empty($leave->leave_detail)){
        $leave->leave_detail = json_decode($leave->leave_detail);
      }
      $employee = Employee::select(['leave_balance'])->where('id','=',$this->user->id)->first();
      return view('leave.edit', compact('pageHeading','leave','employee'));
    }
    else{
      return redirect('leave');
    }
  }

  public function update(Request $request){
    $validate['from_date'] = 'required|date';
    $validate['to_date'] = 'required|date';
    $validate['reason'] = 'required';
    $request->validate($validate);
    if(empty($request->date)){
      return redirect('leave/edit/'.$request->id);
    }
    $update['employee_id'] = $this->user->id;
    $update['from_date'] = $request->from_date;
    $update['to_date'] = $request->to_date;
    $update['leave_status'] = '0';
    $leave_detail = array();
    $leave_count = 0;
    $employee = Employee::select(['leave_balance'])->where('id','=',$this->user->id)->first();
    $availabelLeaves = $employee->leave_balance+$request->leave_count;
    foreach($request->date as $key=>$date){
      $checkLeave = Leave::where('leave_detail', 'LIKE', "%$date%")->where('employee_id','=',$this->user->id)->where('id','!=',$request->id)->get();
      if(count($checkLeave) > 0){
        flash('You have alreay applied for '.$date)->error();
        return redirect('leave/create');
      }
      $sub = array();
      $sub['date'] = $date;
      $sub['leave_type'] = $request->leave_type[$key];
      if($availabelLeaves > $leave_count){
        if($request->leave_type[$key] == 'full'){
          $leave_count = $leave_count+1;
        }
        else{
          $leave_count = $leave_count+0.5;
        }
        $sub['is_paid_leave'] = 1;
      }
      else{
        $sub['is_paid_leave'] = 0;
      }
      $sub['half_period'] = (!empty($request->half_type[$key]))?$request->half_type[$key]:'';
      $sub['is_approved'] = 1;
      array_push($leave_detail, $sub);
    }
    $update['leave_detail'] = json_encode($leave_detail);
    $update['reason'] = $request->reason;
    $update['leave_count'] = $leave_count;
    $update['have_leave_balance'] = $availabelLeaves;
    $empUpdate['leave_balance'] = ($availabelLeaves - $leave_count);
    Leave::whereId($request->id)->update($update);
    Employee::whereId($this->user->id)->update($empUpdate);
    flash('Leave has been updated successfully.')->success();
    return redirect('leave');
  }

  public function leaverequests(){
    $pageHeading = "Leave Requests";
    //$leaves = Leave::orderBy('id', 'DESC')->get();
    $leaves = Leave::leftJoin('employees', function($join){
      $join->on('leaves.employee_id', '=', 'employees.id');
    })
    ->orderBy('leaves.id', 'DESC')->get([
      'leaves.*',
      'employees.name',
      'employees.email',
      'employees.auth_id',
      'employees.designation',
      'employees.leave_balance'
    ]);
    return view('leave.requests', compact('pageHeading','leaves'));
  }

  public function detail($id){
    $pageHeading = "Leave Detail";
    // $leave = Leave::where('id','=',$id)->first();
    $leave = Leave::leftJoin('employees', function($join){
      $join->on('leaves.employee_id', '=', 'employees.id');
    })
    ->where('leaves.id','=',$id)
    ->first([
      'leaves.*',
      'employees.name',
      'employees.email',
      'employees.auth_id',
      'employees.designation',
      'employees.leave_balance'
    ]);
    if($leave){
      if(!empty($leave->leave_detail)){
        $leave->leave_detail = json_decode($leave->leave_detail);
      }
      $employee = Employee::select(['leave_balance'])->where('id','=',$leave->employee_id)->first();
      Notification::where('notification_type','=','leave-apply')->where('to_id','=',0)->where('notification_url','=',route('leave.detail',$leave->id))->update(array('is_read'=>'1'));
      return view('leave.detail', compact('pageHeading','leave','employee'));
    }
    else{
      return redirect('leave');
    }
  }

  public function statusupdate(Request $request){
    $validate['from_date'] = 'required|date';
    $validate['to_date'] = 'required|date';
    $validate['reason'] = 'required';
    $validate['leave_status'] = 'required';
    $request->validate($validate);
    if(empty($request->date)){
      return redirect('leave/detail/'.$request->id);
    }
    $update['from_date'] = $request->from_date;
    $update['to_date'] = $request->to_date;
    $update['leave_status'] = $request->leave_status;
    $employee = Employee::where('id','=',$request->employee_id)->first(['leave_balance','email']);
    if($request->leave_status == 2){
      $update['leave_count'] = 0;
      $empUpdate['leave_balance'] = ($employee->leave_balance+$request->leave_count);
    }
    else{
      $leave_detail = array();
      $leave_count = 0;
      $availabelLeaves = $employee->leave_balance+$request->leave_count;
      foreach($request->date as $key=>$date){
        $checkLeave = Leave::where('leave_detail', 'LIKE', "%$date%")->where('employee_id','=',$request->employee_id)->where('id','!=',$request->id)->get();
        if(count($checkLeave) > 0){
          flash('You have alreay applied for '.$date)->error();
          return redirect('leave/create');
        }
        $sub = array();
        $sub['date'] = $date;
        $sub['leave_type'] = $request->leave_type[$key];
        if($request->is_approved[$key] == 1){
          if($availabelLeaves > $leave_count){
            if($request->leave_type[$key] == 'full'){
              $leave_count = $leave_count+1;
            }
            else{
              $leave_count = $leave_count+0.5;
            }
            $sub['is_paid_leave'] = 1;
          }
          else{
            $sub['is_paid_leave'] = 0;
          }
        }
        $sub['is_approved'] = $request->is_approved[$key];
        $sub['half_period'] = (!empty($request->half_type[$key]))?$request->half_type[$key]:'';
        array_push($leave_detail, $sub);
      }
      $update['leave_detail'] = json_encode($leave_detail);
      $update['reason'] = $request->reason;
      $update['leave_count'] = $leave_count;
      $update['have_leave_balance'] = $availabelLeaves;
      $empUpdate['leave_balance'] = ($availabelLeaves - $leave_count);
    }
    Leave::whereId($request->id)->update($update);
    Employee::whereId($request->employee_id)->update($empUpdate);
    $data = getLeaveDetails($request->id);
    if($request->leave_status == 1){
      Mail::to($employee->email)->send(new LeaveConfirmMail($data));
    }
    else if($request->leave_status == 2){
      Mail::to($employee->email)->send(new LeaveCancelMail($data));
    }
    flash('Leave has been updated successfully.')->success();
    return redirect()->route('leave.leaverequests');
  }

  public function destroy($id){
    $redirectUrl = trim(str_replace(url('/'), '', url()->previous()),'/');
    $leave = Leave::findOrFail($id);
    $employee = Employee::select(['leave_balance'])->where('id','=',$leave->employee_id)->first();
    $leave_balance = ($employee->leave_balance+$leave->leave_count);
    $leave->delete();
    Notification::where('notification_type','=','leave-apply')->where('notification_url','=',route('leave.detail',$id))->update(array('is_read'=>'1'));
    if($leave_balance > 0){
      $this->rebalanceLeaves($leave->employee_id, $leave_balance);
    }
    return redirect($redirectUrl)->with('success', 'Your leave has been deleted as per your request.');
  }

  public function rebalanceLeaves($emp_id = 0, $leave_balance = 0){
    if($emp_id > 0 && $leave_balance > 0){
      $availabelLeaves = $leave_balance;
      $leaves = Leave::where('employee_id','=',$emp_id)->where('leave_status','=','0')->get();
      if(!empty($leaves)){
        foreach($leaves as $key => $leave){
          $availabelLeaves = ($availabelLeaves+$leave->leave_count);
          if($availabelLeaves == 0){
            break;
          }
          $leave_details = json_decode($leave->leave_detail);
          $leave_detail = array();
          $leave_count = 0;
          foreach($leave_details as $key=>$detail){
            $sub = array();
            $sub['date'] = $detail->date;
            $sub['leave_type'] = $detail->leave_type;
            if($availabelLeaves > $leave_count){
              if($detail->leave_type == 'full'){
                $leave_count = $leave_count+1;
              }
              else{
                $leave_count = $leave_count+0.5;
              }
              $sub['is_paid_leave'] = 1;
            }
            else{
              $sub['is_paid_leave'] = 0;
            }
            $sub['half_period'] = (!empty($detail->half_type))?$detail->half_type:'';
            array_push($leave_detail, $sub);
          }
          $update['leave_detail'] = json_encode($leave_detail);
          $update['leave_count'] = $leave_count;
          $availabelLeaves  = ($availabelLeaves - $leave_count);
          Leave::whereId($leave->id)->update($update);
        }
        Employee::whereId($emp_id)->update(array('leave_balance'=>$availabelLeaves));
      }
      else{
        Employee::whereId($emp_id)->update(array('leave_balance'=>$availabelLeaves));
      }
    }
  }
  public function getDateRange(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $dates = getDatesFromRange($startDate, $endDate);
    if(!empty($dates) && count($dates) > 0){
      $string = '';
      foreach($dates as $key=>$date){
        if(getDateTime($date,'D')!="Sat" && getDateTime($date,'D')!="Sun" && $this->isHoliday($date) == false){
          $string .= '<div class="form-group col-sm-4">
          <label for="Documents_Available">Date</label>
          <input class="form-control" name="date['.$key.']" type="text" value="'.$date.'" readonly>
          </div>
          <div class="form-group col-sm-4">
          <label for="Documents_Available">Leave Type</label>
          <select class="form-control leave_type" name="leave_type['.$key.']">
            <option value="full">Full Day</option>
            <option value="half">Half Day</option>
          </select>
          </div>
          <div class="form-group col-sm-4">
          <label for="Documents_Returned">Half Day For</label>
          <select class="form-control half_type" name="half_type['.$key.']" disabled>
            <option value="first half">First Half</option>
            <option value="second half">Second Half</option>
          </select>
          </div>';
        }
      }
    }
    $response['string']  = $string;
    $response['request'] = $request->all();
    sendResponse($response);
  }

  public function getDateRangeWithStatus(Request $request){
    $startDate = $request->startDate;
    $endDate = $request->endDate;
    $dates = getDatesFromRange($startDate, $endDate);
    if(!empty($dates) && count($dates) > 0){
      $string = '';
      foreach($dates as $key=>$date){
        if(getDateTime($date,'D')!="Sat" && getDateTime($date,'D')!="Sun" && $this->isHoliday($date) == false){
          $string .= '<div class="form-group col-sm-3">
          <label for="Documents_Available">Date</label>
          <input class="form-control" name="date['.$key.']" type="text" value="'.$date.'" readonly>
          </div>
          <div class="form-group col-sm-3">
          <label for="Documents_Available">Leave Type</label>
          <select class="form-control leave_type" name="leave_type['.$key.']">
            <option value="full">Full Day</option>
            <option value="half">Half Day</option>
          </select>
          </div>
          <div class="form-group col-sm-3">
          <label for="Documents_Returned">Half Day For</label>
          <select class="form-control half_type" name="half_type['.$key.']" disabled>
            <option value="first half">First Half</option>
            <option value="second half">Second Half</option>
          </select>
          </div>';
        }
      }
    }
    $response['string']  = $string;
    $response['request'] = $request->all();
    sendResponse($response);
  }

  public function isHoliday($date = ''){
    if($date!=""){
      return $holiday = Holiday::where('date','=',$date)->get()->count();
    }
    return false;
  }

  public function getLeaveInfo(Request $request){
    $year = $request->year;
    $month = $request->month;
    $employee_id = $request->employee_id;
    $leaves = Leave::where('leave_status','=','1')->where('leave_detail', 'like', '%'.$year.'-'.$month.'%')->where('employee_id','=',$employee_id)->get();
    if(count($leaves) > 0){
      foreach($leaves as $key=>$leave){
        $this->getPaidUnpaidLeaveCount(json_decode($leave->leave_detail),$year.'-'.$month);
      }
    }
    sendResponse(array("unpaidLeave"=>$this->unpaidLeave,"paidLeave"=>$this->paidLeave,"leave_detail"=>$leaves));
  }

  public function getPaidUnpaidLeaveCount($leave_detail,$yearMonth){
    if(count($leave_detail) > 0){
      foreach($leave_detail as $key=>$leave){
        if(strpos(strval('date'.$leave->date),strval($yearMonth))!=false && $leave->is_approved == 1){
          switch($leave->is_paid_leave){
            case 0:{
              if($leave->leave_type == 'full'){
                $this->unpaidLeave = $this->unpaidLeave+1;
              }
              else{
                $this->unpaidLeave  = $this->unpaidLeave+0.5;
              }
            } break;
            case 1:{
              if($leave->leave_type == 'full'){
                $this->paidLeave = $this->paidLeave+1;
              }
              else{
                $this->paidLeave  = $this->paidLeave+0.5;
              }
            } break;
          }
        }
      }
    }
  }
}
