<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Suggestion;
use App\Notification;
use View;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pageHeading = "Feedback Listing";
        $suggestions = Suggestion::leftJoin('employees', function($join){
            $join->on('suggestions.user_id', '=', 'employees.id');
          })
          ->orderBy('suggestions.id','desc')
          ->select('suggestions.*','employees.name','employees.email','employees.auth_id','employees.designation')
          ->paginate(5);
        return view('suggestion.index', compact('suggestions','pageHeading'))->with('i', (request()->input('page', 1) - 1) *5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $pageHeading = "Add Feedback";
        return view('suggestion.create', compact('pageHeading'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validate['subject'] = 'required';
        $validate['description'] = 'required';
        $request->validate($validate);
        $insert = array();
        $insert['subject'] = $request->subject;
        $insert['description'] = $request->description;
        $insert['user_id'] = $this->user->id;
        $suggestion_id = Suggestion::create($insert)->id;
        $notification['from_id'] = $this->user->id;
        $notification['to_id'] = 0;
        $notification['is_read'] = '0';
        $notification['notification_type'] = 'feedback';
        $notification['notification_url'] = route('suggestion.show',$suggestion_id);
        $notification['message'] = $this->user->name." has given feedback";
        Notification::create($notification);
        return redirect()->back()->with('success', 'Feedback submitted successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $pageHeading = "FeedBack Detail";
        $suggestion = Suggestion::leftJoin('employees', function($join){
            $join->on('suggestions.user_id', '=', 'employees.id');
          })
          ->where('suggestions.id',$id)->first([
            'suggestions.*',
            'employees.name',
            'employees.email',
            'employees.auth_id',
            'employees.designation',
          ]);
          Notification::where('notification_type','=','feedback')->where('to_id','=',0)->where('notification_url','=',route('suggestion.show',$id))->update(array('is_read'=>'1'));
        return view('suggestion.view', compact('suggestion','pageHeading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
      $suggestion = Suggestion::findOrFail($id);
      $suggestion->delete();
      return back()->with('success', 'Holiday has been deleted successfully.');
    }
}
