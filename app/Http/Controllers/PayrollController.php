<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payroll;
use App\Employee;

class PayrollController extends Controller
{
  public function index(){

  }
  public function create($id){
    $employee_id = $id;
    $payrolls = Payroll::where('user_id','=',$employee_id)->get();
    $employee = Employee::where('id','=',$employee_id)->first();
    $pageHeading = $employee->name." Payroll";
    return view('payroll.create', compact('pageHeading','employee','payrolls'));
  }
  public function store(Request $request){
    $date = $request->date;
    $salary = $request->salary;
    $increased_amount = $request->increased_amount;
    $user_id = $request->user_id;
    $validate['salary'] = 'required|numeric';
    $validate['increased_amount'] = 'required|numeric';
    $validate['date'] = 'required|date';
    $validate['user_id'] = 'required|numeric';
    $request->validate($validate);
    $insert['date'] = $date;
    $insert['salary'] = $salary;
    $insert['increased_amount'] = $increased_amount;
    $insert['user_id'] = $user_id;
    $insert['by_id'] = $this->user->id;
    $insert['month'] = getDateTime($date,'m');
    $insert['year'] = getDateTime($date,'Y');
    $isExit = $this->isPayrollExist($insert['user_id'], $insert['month'], $insert['year']);
    if($isExit){
      Payroll::where('user_id','=',$user_id)->where('month','=',$insert['month'])->where('year','=',$insert['year'])->update($insert);
    }
    else{
      Payroll::create($insert);
    }
    return back()->with('success', 'Data has been saved successfully.');
  }
  public function show($id){

  }
  public function edit($id){

  }
  public function update(Request $request, $id){

  }
  public function destroy($id){
    $payroll = Payroll::findOrFail($id);
    $payroll->delete();
    return back()->with('success', 'Data is successfully deleted.');
  }

  private function isPayrollExist($user_id, $month, $year){
    $payroll = Payroll::where('user_id','=',$user_id)->where('month','=',$month)->where('year','=',$year)->first();
    if($payroll){
      return true;
    }
    return false;
  }
}
