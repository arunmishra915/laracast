<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Salary;
use App\Payroll;
use Illuminate\Support\Facades\Session;
use View;
use PDF;
use NumConvert;

class SalaryController extends Controller{
  public function __construct(){
    parent::__construct();
  }

  public function index(){
    $pageHeading = "Salary";
    return view('salary.index', compact('pageHeading'));
  }

  public function getSalaryDetail(Request $request){
    $pageHeading = "Salary Slip";
    if(!empty($request->employee_id)){
      $employee_id = $request->employee_id;
    }
    else{
      $employee_id = $this->user->id;
    }
    $year = $request->year;
    $month = $request->month;
    $employee = Employee::where('id','=',$employee_id)->first();
    $userSalary = Salary::where('month','=',$month)->where('year','=',$year)->where('employee_id','=',$employee_id)->first();
    return view('salary.salaryslip', compact('pageHeading','employee','userSalary'));
  }

  public function overview($id){
    $pageHeading = "Salary Overview";
    $employee = Employee::where('id','=',$id)->first();
    //$userSalaray = $employee->salary;
    $todayDate = getDateTime();
    $userSalaray = $this->getEmployeeSalary($employee->id, getDateTime($todayDate,'m'), getDateTime($todayDate,'Y'));
    $salary['basic'] = ($userSalaray*50)/100;
    $salary['hra'] = ($salary['basic']*50)/100;
    $salary['conveyance'] = 1200;
    $salary['medical_allowance'] = ($userSalaray-($salary['basic']+$salary['hra']+$salary['conveyance']));
    $salary['total'] = ($salary['basic']+$salary['hra']+$salary['conveyance']+$salary['medical_allowance']);
    return view('salary.overview',compact('pageHeading','salary','employee'));
  }

  public function create(){
    $pageHeading = "Create Salary";
    $employees = Employee::where('status','=','active')->get();
    return view('salary.create',compact('pageHeading','employees'));
  }

  public function generateSalary(Request $request){
    $pageHeading = "Salary Slip";
    $validate['employee_id'] = 'required';
    $validate['year'] = 'required';
    $validate['month'] =  'required';
    $request->validate($validate);
    $employee_id = $request->employee_id;
    $year = $request->year;
    $month = $request->month;
    $employee = Employee::where('id','=',$employee_id)->first();
    $userSalary = Salary::where('month','=',$month)->where('year','=',$year)->where('employee_id','=',$employee_id)->first();

    if(!$userSalary){
      $daysInMonth = cal_days_in_month(CAL_GREGORIAN,ltrim($month,0),$year);
      $employee_id = $request->employee_id;
      //$userSalaray = $employee->salary;
      $userSalaray = $this->getEmployeeSalary($employee_id, $month, $year);
      if(!$userSalaray){
        $url = route('payroll.create',$employee_id);
        flash('<strong>Failed</strong> , Unable generate salary first enter salary in <a href="'.$url.'"><strong>payroll section, Click Here</strong></a>.')->error();
        return back()->withInput();
      }
      $salary['employee_id'] = $employee_id;
      $salary['created_by_id'] = $this->user->id;
      $salary['year'] = $year;
      $salary['month'] = $month;
      $salary['date_from'] = getDateTime($year.'-'.$month.'-01','Y-m-d');
      $salary['date_to'] = getDateTime($year.'-'.$month.'-'.$daysInMonth,'Y-m-d');
      $salary['salary_per_month'] = $userSalaray;
      $salary['paid_leaves'] = $request->paid_leaves;
      $salary['unpaid_leaves'] = $request->unpaid_leaves;
      $salary['bonus_amount'] = $request->bonus_amount;
      $salary['working_days'] = $request->working_days;
      $salary['provident_fund'] = 0.00;
      $salary['professional_tax'] = 0.00;
      $salary['unpaid_leaves_amount'] = ($request->unpaid_leaves > 0)?round(($userSalaray/$daysInMonth))*$request->unpaid_leaves:0.00;

      $salary['basic'] = ($userSalaray*50)/100;
      $salary['hra'] = ($salary['basic']*50)/100;
      $salary['conveyance'] = 1200;
      $salary['medical_allowance'] = ($userSalaray-($salary['basic']+$salary['hra']+$salary['conveyance']));
      $salary['total_earning'] = ($salary['basic']+$salary['hra']+$salary['conveyance']+$salary['medical_allowance']+$salary['bonus_amount']);
      $salary['total_deduction'] = ($salary['provident_fund']+$salary['professional_tax']+$salary['unpaid_leaves_amount']);
      $salary['net_salary'] = ($salary['total_earning']-$salary['total_deduction']);
      if(count($salary) > 0){
        Salary::create($salary);
        $userSalary = Salary::where('month','=',$month)->where('year','=',$year)->where('employee_id','=',$employee_id)->first();
      }
    }
    return view('salary.salaryslip', compact('pageHeading','employee','userSalary'));
  }

  public function downloadslip($id){
    $userSalary = Salary::where('id','=',$id)->first();
    if(isset($userSalary)){
      $employee = Employee::where('id','=',$userSalary->employee_id)->first();
      $amountInWords  = NumConvert::word(round($userSalary->net_salary));
      $amountInWords = ucwords(str_replace('-',' ',$amountInWords));
      $pdf = PDF::loadView('salary.pdfsalaryslip', compact('userSalary','employee','amountInWords'));
      $fileName = $employee->name.'_salary_'.$userSalary->year.'-'.$userSalary->month.'.pdf';
      return $pdf->download($fileName);
    }
    else{
      echo 'No Data Avaliable';
    }
  }

  public function destroySlip($id){
    $salary = Salary::findOrFail($id);
    $salary->delete();
    return redirect('salary/create')->with('success', 'Salary slip has been deleted successfully.');
  }

  private function getEmployeeSalary($user_id, $month, $year){
    $daysInMonth = cal_days_in_month(CAL_GREGORIAN,ltrim($month,0),$year);
    $createDate = getDateTime($year.'-'.$month.'-'.$daysInMonth,'Y-m-d');
    //$createDate = $year.'-'.$month.'-07';
    //$payroll = Payroll::where('user_id','=',$user_id)->where('month','<=',$month)->where('year','<=',$year)->orderBy('id','DESC')->take(1)->get(['salary']);
    $payroll = Payroll::where('user_id','=',$user_id)->where('date','<=',$createDate)->orderBy('id','DESC')->take(1)->get(['salary']);
    if($payroll && count($payroll)>0){
      return $payroll[0]->salary;
    }
    return false;
  }

  public function viewSalary($id){
    $pageHeading = "Salary";
    $employee_id = $id;
    return view('salary.index', compact('pageHeading','employee_id'));
  }
}
