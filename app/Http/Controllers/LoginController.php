<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class LoginController extends Controller{

  public function index(){
    $goto = (isset($_GET['goto']) && trim($_GET['goto'])!="")?trim($_GET['goto']):'';
    if(!empty($this->user->id)){
      if($goto!=NULL){
        return redirect($goto);
      }
      else{
        return redirect('dashboard');
      }
    }
    return view('login.index', compact('goto'));
  }

  public function processlogin(Request $request){
    $validate['email'] = 'required|email';
    $validate['password'] = 'required';
    $request->validate($validate);
    $emp = new Employee;
    $employee = $emp->processlogin($request->email);
    if(empty($employee)){
      flash('Invalid email, try again')->error();
      return redirect()->back();
    }
    else{
      $password = $request->password;
      if(Hash::check($password, $employee->password)){
        $request->session()->put('user', $employee);
        flash('<strong>Welcome '.$employee->name.'</strong> , you have login successfully.')->success();
        if($request->goto!=NULL){
          return redirect($request->goto);
        }
        else{
          return redirect('dashboard');
        }
      }
      else{
        flash('Invalid password, try again')->error();
        return redirect()->back();
      }
    }

  }

  public function forgotpassword(){
    return view('login.forgot');
  }

  public function processforgotpass(Request $request){
    $validate['email'] = 'required|email|exists:employees';
    $request->validate($validate);
    $employee = Employee::where('email','=',$request->email)->first();
    $password = str_random(6);
    Employee::whereId($employee->id)->update(array('password'=>Hash::make($password)));
    $data = array(
      'name' => $employee->name,
      'password' => $password
    );
    Mail::to($request->email)->send(new SendMail($data));
    return back()->with('success', 'Thanks we have sent your password!');
  }

  public function logout(Request $request){
    $request->session()->forget('user');
    return redirect('');
  }

  public function adjustLeaveBalance(){
    $today = getDateTime('','Y-m-d');
    $employees = Employee::where('status','active')->get(['id','name','joining_date','leave_balance']);
    foreach($employees as $key=>$employee){
      $monthCount = dateDiffInDays($employee->joining_date, $today);

      if($monthCount > 90){
        Employee::where('id',$employee->id)->update(array("leave_balance"=>($employee->leave_balance+1)));
      }
    }
  }
}
