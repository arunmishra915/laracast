<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use App\Employee;
use View;
class FeedbackController extends Controller{
  public function __construct(){
    parent::__construct();
  }

  public function index(){
    $pageHeading = "Send Feedback";
    $messages = Feedback::leftJoin('employees', function($join){
      $join->on('feedback.message_from', '=', 'employees.id');
    })
    ->where('feedback.employee_id','=',$this->user->id)->orderBy('feedback.created_at','DESC')->take(8)
    ->get([
      'feedback.*',
      'employees.name',
      'employees.image'
    ]);
    Feedback::where('employee_id','=',$this->user->id)->where('message_from','!=',$this->user->id)->update(array('is_read'=>1));
    $messages = $messages->reverse();
    return view('feedback.index', compact('pageHeading','messages'));
  }

  public function store(Request $request){
    $validate['message'] = 'required';
    $request->validate($validate);
    $insert['employee_id'] = $request->employee_id;
    if(!empty($request->message_from)){
      $insert['message_from'] = $request->message_from;
    }
    else{
      $insert['message_from'] = $request->employee_id;
    }
    $insert['message'] = $request->message;
    $insert['is_read'] = 0;
    Feedback::create($insert);
    if(!empty($request->message_from)){
      return redirect('feedback/getfeedback/'.$request->employee_id)->with('success', 'Message has been sent success');
    }
    else{
      return redirect('feedback')->with('success', 'Message has been sent success');
    }
  }

  public function view(){
    $pageHeading = "Employee Feedbacks";
    $employees = Employee::where('status','=','active')->get();
    return view('feedback.view', compact('pageHeading','employees'));
  }

  public function getfeedback($id){
    $pageHeading = "Send Feedback";
    $messages = Feedback::leftJoin('employees', function($join){
      $join->on('feedback.message_from', '=', 'employees.id');
    })
    ->where('feedback.employee_id','=',$id)->orderBy('feedback.created_at','DESC')->take(8)
    ->get([
      'feedback.*',
      'employees.name',
      'employees.image'
    ]);
    Feedback::where('employee_id','=',$id)->where('message_from','=',$id)->update(array('is_read'=>1));
    $data['employee_id'] = $id;
    $messages = $messages->reverse();
    return view('feedback.getfeedback', compact('pageHeading','messages','data'));
  }

  public function getPreviousChat(Request $request){
    $response = array();
    $feedback_id = $request->feedback_id;
    $employee_id = $request->employee_id;
    $is_admin = $request->is_admin;
    $messages = Feedback::leftJoin('employees', function($join){
      $join->on('feedback.message_from', '=', 'employees.id');
    })
    ->where('feedback.employee_id','=',$employee_id)
    ->where('feedback.id','<',$feedback_id)
    ->orderBy('feedback.created_at','DESC')
    ->take(8)
    ->get([
      'feedback.*',
      'employees.name',
      'employees.image'
    ]);
    $messages = $messages->reverse();
    $response['feedback_count'] = (count($messages) > 0)?count($messages):0;
    $response['feedbacks'] = View::make('feedback.previousfeedbackmessages',compact('messages','is_admin'))->render();
    sendResponse($response);
  }
}
