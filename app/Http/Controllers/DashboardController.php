<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Image;

class DashboardController extends Controller{
  public function __construct(){
    parent::__construct();
  }
  public function index(Request $request){
    //print_this($this->user,1);
    // echo $startTime = '2019-11-1 05:00:00';
    // echo '<hr>';
    // echo $cenvertedTime = date('Y-m-d H:i:s',strtotime('-330 min',strtotime($startTime)));die;
    $pageHeading = "Profile";
    $employee  = Employee::findOrFail($this->user->id);
    return view('dashboard.index', compact('pageHeading','employee'));
  }

  public function profile_edit(Request $request){
    $pageHeading = "Profile Edit";
    $employee  = Employee::findOrFail($this->user->id);
    return view('dashboard.profile_edit', compact('pageHeading','employee'));
  }

  public function updateprofile(Request $request){
    $image = $request->file('image');
    //$validate['email'] = 'required|unique:employees|email';
    if(!empty($request->password)){
      $validate['password'] = 'required|min:6|confirmed';
      $validate['password_confirmation'] = 'required|min:6';
    }
    if(!empty($image)){
      $validate['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
    }

    if(!empty($validate)){
      $request->validate($validate);
    }

    $update = array();
    if(!empty($request->password)){
      $update['password'] = Hash::make($request->password);
    }

    if(!empty($image)){
      $update['image'] = time().rand().'-'.str::slug($request->name, '-').'.'.$image->getClientOriginalExtension();
      $destinationPath = public_path('profile/thumbnail');
      $img = Image::make($image->getRealPath());
      $img->resize(100, 100, function ($constraint){
      $constraint->aspectRatio();})->save($destinationPath.'/'.$update['image']);
      $image->move(public_path('profile'), $update['image']);
      if(!empty($request->old_image) && file_exists('public/profile/'.$request->old_image)){
        unlink(public_path('profile/'.$request->old_image));
      }
      if(!empty($request->old_image) && file_exists('public/profile/thumbnail/'.$request->old_image)){
        unlink(public_path('profile/thumbnail/'.$request->old_image));
      }
    }
    Employee::whereId($this->user->id)->update($update);
    $employee = Employee::where('id','=',$this->user->id)->first();
    $request->session()->put('user', $employee);
    flash('<strong>Success</strong> , Your profile has been updateed successfully.')->success();
    return redirect('dashboard');
  }
}
