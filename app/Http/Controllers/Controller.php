<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;
use View;
use App\Notification;
use Illuminate\Support\Facades\Route;
class Controller extends BaseController{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  public function __construct(){
    $this->middleware(function ($request, $next) {
      if($request->session()->has('user')){
        $this->user = $request->session()->get('user');
        View::share('user', $this->user);
        if(in_array($this->user->role,array(1,2))){
          $notifications = Notification::where('is_read','=','0')->where('to_id','=',0)->orderBy('created_at','desc')->get();
        }
        else{
          $notifications = Notification::where('is_read','=','0')->where('to_id','=',$this->user->id)->orderBy('created_at','desc')->get();
        }
        View::share('notifications', $notifications);
      }
      return $next($request);
    });
    $currentAction = Route::currentRouteAction();
    list($controller, $method) = explode('@', $currentAction);
    $controller = explode('Controllers',$controller);
    View::share('className', end($controller));
    View::share('methodName', $method);
  }
}
