<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Employee;
use Validator;

class LoginController extends BaseController{

  public function __construct(){
    parent::__construct();
    $this->response = $this->error = array();
    $this->response['status'] = "0";
  }

  public function processLogin(Request $request){
    $validator = Validator::make($request->all(), [
      'email' => 'required|email',
      'password' => 'required'
    ]);

    if($validator->fails()){
     $errors = json_decode($validator->errors()->toJson(), true);
     if (!empty($errors)){
        foreach($errors as $k => $v) {
          $this->error[] = $v[0];
        }
     }
    }
    else{
      $employee = Employee::where('email','=',$request->email)->first();
      if(!$employee){
        $this->error[] = 'User does not exist.';
      }
      else{
        $password = $request->password;
        if(Hash::check($password, $employee->password) === false){
          $this->error[] = 'Invalid password.';
        }
      }
    }

    if(count($this->error) == 0){
      // $user['id'] = strval($employee->id);
      // $user['name'] = $employee->name;
      // $user['email'] = $employee->email;
      $employee->image = ($employee->image!='' && file_exists('public/profile/thumbnail/'.$employee->image))?url('public/profile/thumbnail/'.$employee->image):url('public/profile/dummy-profile-image.jpg');
      //$user['role'] = strval($employee->role);
      $this->response['status'] = '1';
      $this->response['message'] = 'Login successfully.';
      $this->response['data'] = $employee;
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }

  public function processforgotpass(Request $request){
    $validator = Validator::make($request->all(), [
      'email' => 'required|email|exists:employees',
    ]);
    if($validator->fails()){
     $errors = json_decode($validator->errors()->toJson(), true);
     if (!empty($errors)){
        foreach($errors as $k => $v) {
          $this->error[] = $v[0];
        }
     }
    }
    if(count($this->error) == 0){
      $employee = Employee::where('email','=',$request->email)->first();
      $password = str_random(6);
      Employee::whereId($employee->id)->update(array('password'=>Hash::make($password)));
      $data = array(
        'name' => $employee->name,
        'password' => $password
      );
      Mail::to($request->email)->send(new SendMail($data));
      $this->response['status'] = '1';
      $this->response['message'] = 'Thanks we have sent your password!';
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }
}
