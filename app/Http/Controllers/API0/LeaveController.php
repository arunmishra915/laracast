<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Employee;
use App\Holiday;
use App\Leave;
use App\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\LeaveApplyMail;
use Validator;

class LeaveController extends BaseController{
  public function __construct(){
    parent::__construct();
    $this->response = $this->error = array();
    $this->response['status'] = "0";
  }

  // public function getDateRange(Request $request){
  //   $startDate = $request->startDate;
  //   $endDate = $request->endDate;
  //   $dates = getDatesFromRange($startDate, $endDate);
  //   if(!empty($dates) && count($dates) > 0){
  //     $date_range = array();
  //     foreach($dates as $key=>$date){
  //       if(getDateTime($date,'D')!="Sat" && getDateTime($date,'D')!="Sun" && $this->isHoliday($date) == false){
  //         $range['date'] = $date;
  //         $range['key'] = $key;
  //         array_push($date_range,$range);
  //       }
  //     }
  //   }
  //   $response['date_range']  = $date_range;
  //   sendResponse($response);
  // }
  public function getDateRange($startDate, $endDate){
    $dates = getDatesFromRange($startDate, $endDate);
    if(!empty($dates) && count($dates) > 0){
      $date_range = array();
      foreach($dates as $key=>$date){
        if(getDateTime($date,'D')!="Sat" && getDateTime($date,'D')!="Sun" && $this->isHoliday($date) == false){
          $range['date'] = $date;
          $range['leave_type'] = 'full';
          $range['half_type'] = '';
          array_push($date_range,$range);
        }
      }
    }
    return $date_range;
  }

  public function isHoliday($date = ''){
    if($date!=""){
      return $holiday = Holiday::where('date','=',$date)->get()->count();
    }
    return false;
  }

  public function submitLeave(Request $request){
    $insert['employee_id'] = isset($request->user_id)?$request->user_id:0;
    $insert['from_date'] = $request->from_date;
    $insert['to_date'] = $request->to_date;
    $insert['leave_status'] = '0';
    $leave_detail = array();
    $leave_count = 0;
    if($insert['employee_id'] == 0){
      $this->error[] = "Invalid request.";
    }

    $results = $this->getDateRange($request->from_date, $request->to_date);
    foreach($results as $key=>$result){
      $checkLeave = Leave::where('leave_detail', 'LIKE', "%{$result['date']}%")->where('employee_id','=',$request->user_id)->get();
      if(count($checkLeave) > 0){
        $this->error[] = 'You have alreay applied for '.$result['date'];
      }
    }
    if(count($this->error) == 0){
      $employee = Employee::select(['name','leave_balance'])->where('id','=',$request->user_id)->first();
      $availabelLeaves = $employee->leave_balance;

      foreach($results as $key=>$result){
        $sub = array();
        $sub['date'] = $result['date'];
        $sub['leave_type'] = $result['leave_type'];
        if($availabelLeaves > $leave_count){
          if($result['leave_type'] == 'full'){
            $leave_count = $leave_count+1;
          }
          else{
            $leave_count = $leave_count+0.5;
          }
          $sub['is_paid_leave'] = 1;
        }
        else{
          $sub['is_paid_leave'] = 0;
        }
        $sub['half_period'] = (!empty($result['half_type']))?$result['half_type']:'';
        array_push($leave_detail, $sub);
      }
      $insert['leave_detail'] = json_encode($leave_detail);
      $insert['reason'] = $request->reason;
      $insert['leave_count'] = $leave_count;
      $update['leave_balance'] = ($employee->leave_balance - $leave_count);
      $leave_id = Leave::create($insert)->id;
      Employee::whereId($request->user_id)->update($update);
      $notification['from_id'] = $request->user_id;
      $notification['to_id'] = 0;
      $notification['is_read'] = '0';
      $notification['notification_type'] = 'leave-apply';
      $notification['notification_url'] = route('leave.detail',$leave_id);
      $notification['message'] = $employee->name." has applied for leaves";
      Notification::create($notification);
      $data = array(
        'name' => $employee->name,
        'email' => $request->email,
        'from_date' => $request->from_date,
        'to_date' => $request->to_date,
        'reason' => $request->reason,
        'leave_detail' => $leave_detail
      );
      Mail::to('sudhanshu+6000@delimp.com')->send(new LeaveApplyMail($data));
      $this->response['status'] = '1';
      $this->response['message'] = 'Leave has been applied successfully.';
    }
    else{
      $this->response['message'] = $this->error;
    }
    sendResponse($this->response);
  }

  public function myLeaves(Request $request){
    $employee_id = $request->user_id;
    $employee = Employee::select(['leave_balance'])->where('id','=',$employee_id)->orderBy('created_at','DESC')->first();
    $leaves = Leave::where('employee_id','=',$employee_id)->get();
    $this->response['leave_balance'] = $employee->leave_balance;
    if(count($leaves) > 0){
      foreach($leaves as $key=>$leave){
        switch($leave->leave_status){
          case 0:{
            $leaves[$key]->leave_status = "Pending";
          } break;
          case 1:{
            $leaves[$key]->leave_status = "Approved";
          } break;
          default:{
            $leaves[$key]->leave_status =  "Canceled";
          }
        }
      }
      $this->response['status'] = "1";
      $this->response['leaves'] = $leaves;
    }
    else{
      $this->response['message'] = "There no data available!";
    }
    sendResponse($this->response);
  }

  // public function store(Request $request){
  //   $insert['employee_id'] = $request->user_id;
  //   $insert['from_date'] = $request->from_date;
  //   $insert['to_date'] = $request->to_date;
  //   $insert['leave_status'] = '0';
  //   $leave_detail = array();
  //   $leave_count = 0;
  //   $employee = Employee::select(['name','leave_balance'])->where('id','=',$request->user_id)->first();
  //   $availabelLeaves = $employee->leave_balance;
  //   foreach($request->date as $key=>$date){
  //     $checkLeave = Leave::where('leave_detail', 'LIKE', "%$date%")->where('employee_id','=',$request->user_id)->get();
  //     if(count($checkLeave) > 0){
  //       $this->error[] = 'You have alreay applied for '.$date;
  //     }
  //   }
  //   if(count($this->error) == 0){
  //     foreach($request->date as $key=>$date){
  //       $sub = array();
  //       $sub['date'] = $date;
  //       $sub['leave_type'] = $request->leave_type[$key];
  //       if($availabelLeaves > $leave_count){
  //         if($request->leave_type[$key] == 'full'){
  //           $leave_count = $leave_count+1;
  //         }
  //         else{
  //           $leave_count = $leave_count+0.5;
  //         }
  //         $sub['is_paid_leave'] = 1;
  //       }
  //       else{
  //         $sub['is_paid_leave'] = 0;
  //       }
  //       $sub['half_period'] = (!empty($request->half_type[$key]))?$request->half_type[$key]:'';
  //       array_push($leave_detail, $sub);
  //     }
  //     $insert['leave_detail'] = json_encode($leave_detail);
  //     $insert['reason'] = $request->reason;
  //     $insert['leave_count'] = $leave_count;
  //     $update['leave_balance'] = ($employee->leave_balance - $leave_count);
  //     $leave_id = Leave::create($insert)->id;
  //     Employee::whereId($request->user_id)->update($update);
  //     $notification['from_id'] = $request->user_id;
  //     $notification['to_id'] = 0;
  //     $notification['is_read'] = '0';
  //     $notification['notification_type'] = 'leave-apply';
  //     $notification['notification_url'] = route('leave.detail',$leave_id);
  //     $notification['message'] = $employee->name." has applied for leaves";
  //     Notification::create($notification);
  //     $this->response['message'] = 'Leave has been applied successfully.';
  //   }
  //   else{
  //     $this->response['message'] = $this->error;
  //   }
  //   sendResponse($response);
  // }

  public function deleteLeave(Request $request){
    $leave_id = (isset($request->leave_id))?$request->leave_id:0;
    $user_id = (isset($request->user_id))?$request->user_id:0;

    if($leave_id == 0 || $user_id == 0){
      $this->error[] = "Provide all requird parameters.";
    }
    if(!$this->leaveExit($leave_id, $user_id)){
      $this->error[] = "Invalid request.";
    }
    else{
      if(!$this->isLeavePending($leave_id, $user_id)){
        $this->error[] = "Leave is not in pending state.";
      }
    }

    if(count($this->error) == 0){
      $leave = Leave::findOrFail($leave_id);
      $employee = Employee::select(['leave_balance'])->where('id','=',$leave->employee_id)->first();
      $leave_balance = ($employee->leave_balance+$leave->leave_count);
      $leave->delete();
      Notification::where('notification_type','=','leave-apply')->where('notification_url','=',route('leave.detail',$leave_id))->update(array('is_read'=>'1'));
      if($leave_balance > 0){
        $this->rebalanceLeaves($leave->employee_id, $leave_balance);
      }
      $this->response['status'] = "1";
      $this->response['message']= "Your leave has been deleted as per your request.";
    }
    else{
      $this->response['message']= $this->error;
    }
    sendResponse($this->response);
  }

  public function leaveExit($leave_id, $user_id){
    $leave = Leave::where('id',$leave_id)->where('employee_id', $user_id)->get();
    return $leave->count();
  }

  public function isLeavePending($leave_id, $user_id){
    $leave = Leave::where('id',$leave_id)->where('employee_id', $user_id)->where('leave_status','0')->get();
    return $leave->count();
  }

  public function rebalanceLeaves($emp_id = 0, $leave_balance = 0){
    if($emp_id > 0 && $leave_balance > 0){
      $availabelLeaves = $leave_balance;
      $leaves = Leave::where('employee_id','=',$emp_id)->where('leave_status','=','0')->get();
      if(!empty($leaves)){
        foreach($leaves as $key => $leave){
          $availabelLeaves = ($availabelLeaves+$leave->leave_count);
          if($availabelLeaves == 0){
            break;
          }
          $leave_details = json_decode($leave->leave_detail);
          $leave_detail = array();
          $leave_count = 0;
          foreach($leave_details as $key=>$detail){
            $sub = array();
            $sub['date'] = $detail->date;
            $sub['leave_type'] = $detail->leave_type;
            if($availabelLeaves > $leave_count){
              if($detail->leave_type == 'full'){
                $leave_count = $leave_count+1;
              }
              else{
                $leave_count = $leave_count+0.5;
              }
              $sub['is_paid_leave'] = 1;
            }
            else{
              $sub['is_paid_leave'] = 0;
            }
            $sub['half_period'] = (!empty($detail->half_type))?$detail->half_type:'';
            array_push($leave_detail, $sub);
          }
          $update['leave_detail'] = json_encode($leave_detail);
          $update['leave_count'] = $leave_count;
          $availabelLeaves  = ($availabelLeaves - $leave_count);
          Leave::whereId($leave->id)->update($update);
        }
        Employee::whereId($emp_id)->update(array('leave_balance'=>$availabelLeaves));
      }
      else{
        Employee::whereId($emp_id)->update(array('leave_balance'=>$availabelLeaves));
      }
    }
  }
}
