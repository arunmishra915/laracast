<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Employee;
use App\Holiday;
use App\Policy;
use App\Salary;
use App\Suggestion;
use App\Notification;
use Validator;
use Image;
use NumConvert;
use PDF;

class UserController extends BaseController{
  public function __construct(){
    parent::__construct();
    $this->response = $this->error = array();
    $this->response['status'] = "0";
  }

  public function getUserDetail(Request $request){
    if(empty($request->user_id)){
      $this->error[] = 'Invalid Request.';
    }
    else{
      $user = Employee::where('id','=',$request->user_id)->first();
      if(!$user){
        $this->error[] = 'User does not exist.';
      }
      else{
        $data = array();
        // $data['name'] = $user->name;
        // $data['email'] = $user->email;
        $user->image = ($user->image!='' && file_exists('public/profile/thumbnail/'.$user->image))?url('public/profile/thumbnail/'.$user->image):url('public/profile/dummy-profile-image.jpg');
        // $data['designation'] = $user->designation;
        // $data['contact_number'] = $user->contact_number;
        // $data['emergency_contact_number'] = $user->emergency_contact_number;
        // $data['dob'] = getDateTime($user->dob, 'd F Y');
        // $data['joining_date'] = $user->joining_date;
        // $data['status'] = $user->status;
        // $data['document_available'] = $user->document_available;
        // $data['document_returned'] = $user->document_returned;
      }
    }
    if(count($this->error) == 0){
      $this->response['status'] = "1";
      $this->response['message'] = "User data found.";
      $this->response['data'] = $user;
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }

  public function getHoliday(){
    $this->response['normalHolidays'] = Holiday::where('type','=',1)->whereYear('date', date('Y'))->orderBy('date','ASC')->get(['holiday','date']);
    $this->response['restrictedHolidays'] = Holiday::where('type','=',2)->whereYear('date', date('Y'))->orderBy('date','ASC')->get(['holiday','date']);
    if(count($this->response['normalHolidays']) > 0){
      $this->response['status'] = "1";
      $this->response['message'] = "data found.";
    }
    else{
      $this->response['message'] = "data not found.";
    }
    sendResponse($this->response);
  }

  public function getPolicyData(){
    $policy = Policy::where('id','=',1)->first(['title','content']);
    $this->response['status'] = "1";
    $this->response['policy'] = $policy;
    sendResponse($this->response);
  }
  public function updateProfile(Request $request){
    //$image = $request->file('image');
    $image = $request->image;
    if(!empty($request->password)){
      $validate['password'] = 'required|min:6|confirmed';
      $validate['password_confirmation'] = 'required|min:6';
    }
    if(!empty($image)){
      //$validate['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
    }
    $validate['user_id'] = 'required';
    $validator = Validator::make($request->all(), $validate);

    if($validator->fails()){
     $errors = json_decode($validator->errors()->toJson(), true);
     if (!empty($errors)){
        foreach($errors as $k => $v) {
          foreach($v as $error){
            $this->error[] = $error;
          }
        }
     }
    }

    if(count($this->error) == 0){
      $update = array();
      if(!empty($request->password)){
        $update['password'] = Hash::make($request->password);
      }

      if(!empty($image)){
        $update['image'] = time().rand().'-'.str::slug($request->user_id, '-').'.jpg';
        // $destinationPath = public_path('profile/thumbnail');
        // $img = Image::make($image->getRealPath());
        // $img->resize(100, 100, function ($constraint){
        // $constraint->aspectRatio();})->save($destinationPath.'/'.$update['image']);
        // $image->move(public_path('profile'), $update['image']);
        $user = Employee::where('id','=',$request->user_id)->first();
        if(!empty($user->image) && file_exists('public/profile/'.$user->image)){
          unlink(public_path('profile/'.$user->image));
        }
        if(!empty($user->image) && file_exists('public/profile/thumbnail/'.$user->image)){
          unlink(public_path('profile/thumbnail/'.$user->image));
        }
        file_put_contents('public/profile/thumbnail/'.$update['image'], base64_decode($image));
        file_put_contents('public/profile/'.$update['image'], base64_decode($image));
      }
      if(count($update) > 0){
        Employee::whereId($request->user_id)->update($update);
      }
      $employee = Employee::where('id','=',$request->user_id)->first();
      $employee->image = ($employee->image!='' && file_exists('public/profile/thumbnail/'.$employee->image))?url('public/profile/thumbnail/'.$employee->image):url('public/profile/dummy-profile-image.jpg');
      $this->response['status'] = "1";
      $this->response['data'] = $employee;
      $this->response['message'] = "Profile has been updated successfully.";
    }
    else{
      $this->response['error'] = $this->error;
    }
    //sendResponse($this->response);
    sendResponse($this->response);
  }
  public function updateProfile_2(Request $request){
    $image = $request->file('image');
    if(!empty($request->password)){
      $validate['password'] = 'required|min:6|confirmed';
      $validate['password_confirmation'] = 'required|min:6';
    }
    if(!empty($image)){
      $validate['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
    }
    $validate['user_id'] = 'required';
    $validator = Validator::make($request->all(), $validate);

    if($validator->fails()){
     $errors = json_decode($validator->errors()->toJson(), true);
     if (!empty($errors)){
        foreach($errors as $k => $v) {
          foreach($v as $error){
            $this->error[] = $error;
          }
        }
     }
    }

    if(count($this->error) == 0){
      $update = array();
      if(!empty($request->password)){
        $update['password'] = Hash::make($request->password);
      }

      if(!empty($image)){
        $update['image'] = time().rand().'-'.str::slug($request->name, '-').'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('profile/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint){
        $constraint->aspectRatio();})->save($destinationPath.'/'.$update['image']);
        $image->move(public_path('profile'), $update['image']);
        $user = Employee::where('id','=',$request->user_id)->first();
        if(!empty($user->image) && file_exists('public/profile/'.$user->image)){
          unlink(public_path('profile/'.$user->image));
        }
        if(!empty($user->image) && file_exists('public/profile/thumbnail/'.$user->image)){
          unlink(public_path('profile/thumbnail/'.$user->image));
        }
      }
      if(count($update) > 0){
        Employee::whereId($request->user_id)->update($update);
      }
      $this->response['status'] = "1";
      $this->response['message'] = "Profile has been updated successfully.";
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }

  public function getSalaryDetail(Request $request){
    $employee_id = $request->user_id;
    $year = $request->year;
    $month = $request->month;
    $validate['user_id'] = 'required|numeric';
    $validate['year'] = 'required|numeric';
    $validate['month'] = 'required|numeric';

    $validator = Validator::make($request->all(), $validate);
    if($validator->fails()){
      $errors = json_decode($validator->errors()->toJson(), true);
      if(!empty($errors)){
        foreach($errors as $k => $v) {
          foreach($v as $error){
            $this->error[] = $error;
          }
        }
      }
    }

    if(count($this->error) == 0){
      $employee = Employee::where('id','=',$employee_id)->first();
      $userSalary = Salary::where('month','=',$month)->where('year','=',$year)->where('employee_id','=',$employee_id)->first();
      if($userSalary!=NULL){
        $this->response['status'] = "1";
        $this->response['message'] = "Data Found.";
        $this->response['employee'] = $employee;
        $this->response['salary'] = $userSalary;
        $this->response['slip_url'] = url('api/downloadslip',$userSalary->id);
      }
      else{
        $this->response['message'] = "Data not available.";
      }
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }

  public function downloadSlip($id){
    $userSalary = Salary::where('id','=',$id)->first();
    if(isset($userSalary)){
      $employee = Employee::where('id','=',$userSalary->employee_id)->first();
      $amountInWords  = NumConvert::word($userSalary->net_salary);
      $pdf = PDF::loadView('salary.pdfsalaryslip', compact('userSalary','employee','amountInWords'));
      $fileName = $employee->name.'_salary_'.$userSalary->year.'-'.$userSalary->month.'.pdf';
      return $pdf->download($fileName);
    }
    else{
      echo 'No Data Avaliable';
    }
  }

  public function sendFeedback(Request $request){
    $user_id = (isset($request->user_id))?$request->user_id:0;
    $validate['user_id'] = 'required|numeric';
    $validate['subject'] = 'required';
    $validate['description'] = 'required';
    $validator = Validator::make($request->all(), $validate);
    if($validator->fails()){
      $errors = json_decode($validator->errors()->toJson(), true);
      if(!empty($errors)){
        foreach($errors as $k => $v) {
          foreach($v as $error){
            $this->error[] = $error;
          }
        }
      }
    }
    if(count($this->error) == 0){
      $insert['subject'] = $request->subject;
      $insert['description'] = $request->description;
      $insert['user_id'] = $request->user_id;
      $employee = Employee::where('id','=',$request->user_id)->first();
      $suggestion_id = Suggestion::create($insert)->id;
      if($suggestion_id){
        $notification['from_id'] = $request->user_id;
        $notification['to_id'] = 0;
        $notification['is_read'] = '0';
        $notification['notification_type'] = 'feedback';
        $notification['notification_url'] = route('suggestion.show',$suggestion_id);
        $notification['message'] = $employee->name." has given feedback";
        Notification::create($notification);
        $this->response['status'] = "1";
        $this->response['message'] = "Feedback sent successfully.";
      }
      else{
        $this->response['message'] = "Data not available.";
      }
    }
    else{
      $this->response['error'] = $this->error;
    }
    sendResponse($this->response);
  }
}
