<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Policy;
use App\Employee;
use Illuminate\Support\Facades\Mail;
use App\Mail\PolicyNotificationMail;

class PolicyController extends Controller{
  public function __construct(){
    parent::__construct();
  }

  public function index(){
    $pageHeading = "Company Policy";
    $policy = Policy::where('id','=',1)->first();
    return view('policy.index', compact('pageHeading','policy'));
  }

  public function edit(){
    $pageHeading = "Edit Company Policy";
    $policy = Policy::where('id','=',1)->first();
    return view('policy.edit', compact('pageHeading','policy'));
  }

  public function update(Request $request){
    $validate['content'] = 'required';
    $request->validate($validate);
    Policy::whereId(1)->update(array('content'=>$request->content));
    if(!empty($request->send_notification)){
      $request->send_notification;
      $data = array(
        'url' => url('/').'?goto='.url('policy'),
        'content' => $request->content
      );
      $emails = getActiveEmails();
      if(count($emails) > 0){
        Mail::to($emails)->send(new PolicyNotificationMail($data));
      }
    }
    flash('Policy Content has been updated successfully.')->success();
    return redirect()->route('policy.index');
  }
}
