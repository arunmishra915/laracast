<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Document;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendRegistrationMail;
use Illuminate\Support\Facades\Session;
use Image;

class EmployeeController extends Controller{
    public function __construct(){
      parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $pageHeading = "Employee Management";
      $employees = Employee::latest()->paginate(10);
      return view('employee.index', compact('employees','pageHeading'))->with('i', (request()->input('page', 1) - 1) *10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
      $pageHeading = "Add Employee";
      return view('employee.create', compact('pageHeading'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $image = $request->file('image');
        $validate['name'] = 'required';
        $validate['designation'] = 'required';
        $validate['contact_number'] = 'required|numeric';
        $validate['emergency_contact_number'] = 'required|numeric';
        $validate['dob'] = 'required|date';
        $validate['joining_date'] = 'required|date';
        //$validate['leave_balance'] = 'required';
        // $validate['salary'] = 'required';
        $validate['bank_account'] = 'required';
        $validate['bank_name'] = 'required';
        $validate['ifcs_code'] = 'required';
        $validate['pancard'] = 'required';
        $validate['document_available'] = 'required';
        $validate['document_returned'] = 'required';
        $validate['status'] = 'required';
        $validate['email'] = 'required|unique:employees|email';
        $validate['auth_id'] = 'required|unique:employees';
        $validate['password'] = 'required';
        if(!empty($image)){
          $validate['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

      $request->validate($validate);
      $insert = array();
      $insert['name'] = $request->name;
      $insert['email'] = $request->email;
      $insert['auth_id'] = $request->auth_id;
      $insert['password'] = Hash::make($request->password);
      $insert['role'] = $request->role;
      $insert['designation'] = $request->designation;
      $insert['contact_number'] = $request->contact_number;
      $insert['emergency_contact_number'] = $request->emergency_contact_number;
      $insert['dob'] = getDateTime($request->dob, 'Y-m-d');
      $insert['joining_date'] = getDateTime($request->joining_date, 'Y-m-d');
      $insert['leave_balance'] = $request->leave_balance;
      // $insert['salary'] = $request->salary;
      $insert['bank_account'] = $request->bank_account;
      $insert['bank_name'] = $request->bank_name;
      $insert['ifcs_code'] = $request->ifcs_code;
      $insert['pancard'] = $request->pancard;
      $insert['document_available'] = $request->document_available;
      $insert['document_returned'] = $request->document_returned;
      $insert['status'] = $request->status;

      if(!empty($image)){
        $insert['image'] = time().rand().'-'.str::slug($request->name, '-').'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('profile/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint){
        $constraint->aspectRatio();})->save($destinationPath.'/'.$insert['image']);
        $image->move(public_path('profile'), $insert['image']);
      }
      Employee::create($insert);
      $data = array(
        'name' => $request->name,
        'email' => $request->email,
        'password' => $request->password,
      );
      Mail::to($request->email)->send(new SendRegistrationMail($data));
      return redirect('employee')->with('success', 'Employee Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
      $pageHeading = "Employee Detail";
      $employee = Employee::findOrFail($id);
      // if(empty($employee->image) || !file_exists('public/profile/'.$employee->image)){
      //   $employee->image = 'dummy-profile-image.jpg';
      // }
      return view('employee.view', compact('employee','pageHeading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
      $pageHeading = "Update Employee";
      $employee = Employee::findOrFail($id);
      return view('employee.edit', compact('employee','pageHeading'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
      $image = $request->file('image');
      $validate['name'] = 'required';
      $validate['designation'] = 'required';
      $validate['contact_number'] = 'required|numeric';
      $validate['emergency_contact_number'] = 'required|numeric';
      $validate['dob'] = 'required|date';
      $validate['joining_date'] = 'required|date';
      //$validate['leave_balance'] = 'required';
      // $validate['salary'] = 'required';
      $validate['bank_account'] = 'required';
      $validate['bank_name'] = 'required';
      $validate['ifcs_code'] = 'required';
      $validate['pancard'] = 'required';
      $validate['document_available'] = 'required';
      $validate['document_returned'] = 'required';
      $validate['status'] = 'required';
      $validate['email'] = 'required|email|unique:employees,email,'.$id;
      $validate['auth_id'] = 'required|unique:employees,auth_id,'.$id;
      if(!empty($image)){
        $validate['image'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
      }
      if($request->document_returned == 'yes'){
        $validate['document_returned_date'] = 'required|date';
      }

      $request->validate($validate);
      $update = array();
      $update['name'] = $request->name;
      $update['email'] = $request->email;
      $update['auth_id'] = $request->auth_id;
      if(!empty($request->password)){
        $update['password'] = Hash::make($request->password);
      }
      $update['role'] = $request->role;
      $update['designation'] = $request->designation;
      $update['contact_number'] = $request->contact_number;
      $update['emergency_contact_number'] = $request->emergency_contact_number;
      $update['dob'] = getDateTime($request->dob, 'Y-m-d');
      $update['joining_date'] = getDateTime($request->joining_date, 'Y-m-d');
      $update['leave_balance'] = $request->leave_balance;
      // $update['salary'] = $request->salary;
      $update['bank_account'] = $request->bank_account;
      $update['bank_name'] = $request->bank_name;
      $update['ifcs_code'] = $request->ifcs_code;
      $update['pancard'] = $request->pancard;
      $update['document_available'] = $request->document_available;
      $update['document_returned'] = $request->document_returned;
      if($request->document_returned == 'yes'){
        $update['document_returned_date'] = $request->document_returned_date;
      }
      $update['status'] = $request->status;
      if(!empty($image)){
        $update['image'] = time().rand().'-'.str::slug($request->name, '-').'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('profile/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint){
        $constraint->aspectRatio();})->save($destinationPath.'/'.$update['image']);
        $image->move(public_path('profile'), $update['image']);
        if(!empty($request->old_image) && file_exists('public/profile/'.$request->old_image)){
          unlink(public_path('profile/'.$request->old_image));
        }
        if(!empty($request->old_image) && file_exists('public/profile/thumbnail/'.$request->old_image)){
          unlink(public_path('profile/thumbnail/'.$request->old_image));
        }
      }
       Employee::whereId($id)->update($update);
      return redirect('employee')->with('success', 'Employee is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
      $employee = Employee::findOrFail($id);
      $documents = Document::where('employee_id', $id)->get();
      if(!empty($documents)){
        foreach($documents as $document){
          unlink(public_path('employee_documents/'.$document->document));
          $document->delete();
        }
      }
      if(!empty($employee->image)  && file_exists('public/profile/'.$employee->image)){
        unlink(public_path('profile/'.$employee->image));
        if(file_exists('public/profile/thumbnail/'.$employee->image)){
          unlink(public_path('profile/thumbnail/'.$employee->image));
        }
      }
      $employee->delete();
      return redirect('employee')->with('success', 'Employee is successfully deleted.');
    }

    public function document($id){
      $pageHeading = "Documents";
      $data['employee_id'] = $id;
      $data['documents'] = Document::where('employee_id', $id)->get();
      return view('employee.documents', compact('data','pageHeading'));
    }

    public function documentupload(Request $request){
      $employeeId = $request->employee_id;
      $document_discription = $request->document_discription;
      $images = $request->file('document_image');
      if(is_array($images)){
        foreach($images as $key=>$image){
          $insert = array();
          $insert['document'] = 'empId'.$employeeId.'-'.time().rand().'-'.Str::slug($document_discription[$key],'-').'.'.$image->getClientOriginalExtension();
          $insert['document_discription'] = $document_discription[$key];
          $insert['employee_id'] = $employeeId;
          $image->move(public_path('employee_documents'), $insert['document']);
          Document::create($insert);
        }
        return redirect('employee/document/'.$employeeId)->with('success', 'Employee Documents Added successfully.');
      }
    }

    public function document_edit($id){
      $pageHeading = "Documents";
      $document = Document::findOrFail($id);
      return view('employee.document_edit', compact('document','pageHeading'));
    }

    public function document_update(Request $request){
      $document_image = $request->file('document_image');
      $employeeId = $request->employee_id;
      $update['document_discription'] = $request->document_discription;
      $update['employee_id'] = $employeeId;
      if(!empty($document_image)){
        $update['document'] = 'empId'.$employeeId.'-'.time().rand().'-'.Str::slug($update['document_discription'],'-').'.'.$document_image->getClientOriginalExtension();
        $document_image->move(public_path('employee_documents'), $update['document']);
        unlink(public_path('employee_documents/'.$request->old_document));
      }
      Document::whereId($request->id)->update($update);
      return redirect('employee/document_edit/'.$request->id)->with('success', 'Employee Document is successfully updated');
    }

    public function document_destroy($id){
      $document = Document::findOrFail($id);
      unlink(public_path('employee_documents/'.$document->document));
      $document->delete();
      return redirect('employee/document/'.$document->employee_id)->with('success', 'Employee Document is successfully deleted.');
    }

    public function dashboard(){
      echo 'employee_dashboard';
      // $user = $request->session()->get('user');
      // print_this($user);
    }
}
