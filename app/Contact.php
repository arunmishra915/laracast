<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Contact extends Model
{
  public function getContactDetail($id){
    return $users = DB::table('contacts')->find($id);
  }

  public function updateContactDetail($data){
    $id = $data['id'];
    return $result = DB::table('contacts')->where('id','=',$id)->update($data);
  }

  public function deleteContactDetail($id){
    return $result = DB::table('contacts')->where('id','=',$id)->delete();
  }
}
