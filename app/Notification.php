<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model{
  protected $fillable = ['from_id','to_id','is_read','notification_type','notification_url','message'];
}
