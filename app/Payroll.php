<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model{
  protected $fillable = ['user_id','by_id','salary','month','year','date','increased_amount'];
}
