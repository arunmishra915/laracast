<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Employee;

class DummyCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $today = getDateTime('','Y-m-d');
      $employees = Employee::where('status','active')->get(['id','name','joining_date','leave_balance']);
      foreach($employees as $key=>$employee){
        $monthCount = dateDiffInDays($employee->joining_date, $today);

        if($monthCount > 90){
          Employee::where('id',$employee->id)->update(array("leave_balance"=>($employee->leave_balance+1)));
        }
      }
    }
}
