<?php
function ip_address(){
  return $_SERVER['REMOTE_ADDR'];
}

function from_device(){
  return $_SERVER['HTTP_USER_AGENT'];
}

function sendResponse($data=array()){
  header('Content-Type: application/json');
  echo json_encode($data);
  exit();
}
function print_this($array,$flag=0){
	echo '<pre>';
	print_r($array);
	if($flag == 1){
		die;
	}
}
function getDateTime($datetime='',$format='Y-m-d H:i:s') {
	$format = trim($format)=='' ? 'Y-m-d H:i:s' : $format;
	$datetime = (trim($datetime)=='') ? date($format) : $datetime;
	return date($format,strtotime($datetime));
}

function dateDiffInDays($date1, $date2){
  $diff = strtotime($date2) - strtotime($date1);
  return abs(round($diff / 86400));
}

function getDatesFromRange($start, $end, $format = 'Y-m-d') {
  $array = array();
  $interval = new DateInterval('P1D');
  $realEnd = new DateTime($end);
  $realEnd->add($interval);
  $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
  foreach($period as $date) {
    $array[] = $date->format($format);
  }
  return $array;
}

if (!function_exists('uploadDocs')) {

    function uploadPics($fieldName,$uploadPath = "",$slug = "") {
        $CI = setProtocol();
        $CI->load->helper('string');
        if($uploadPath!=""){
          $config['upload_path'] = $uploadPath;
        }
        else{
          $config['upload_path'] = MODEL_IMAGE_UPLOAD_LOCATION;
        }
        $config['allowed_types'] = 'gif|jpg|png';
        if($slug!=""){
          $config['file_name'] = time() ."_".$slug."_" . random_string('alnum', 15);
        }
        else{
          $config['file_name'] = time() ."_" . random_string('alnum', 15);
        }
        $CI->load->library('upload', $config);
        if (!$CI->upload->do_upload($fieldName)) {
            $rt['status'] = 'error';
            $rt['message'] = strip_tags($CI->upload->display_errors());
        } else {
            $rt['status'] = 'success';
            $data = $CI->upload->data();
            $rt['data'] = $data;
        }
        return $rt;
    }

}

if (!function_exists('setProtocol')) {

  function setProtocol() {
      $CI = &get_instance();
      $CI->load->library('email');
      $config['protocol'] = PROTOCOL;
      $config['mailpath'] = MAIL_PATH;
      $config['smtp_host'] = SMTP_HOST;
      $config['smtp_port'] = SMTP_PORT;
      $config['smtp_user'] = SMTP_USER;
      $config['smtp_pass'] = SMTP_PASS;
      $config['charset'] = "utf-8";
      $config['mailtype'] = "html";
      $config['newline'] = "\r\n";
      $CI->email->initialize($config);
      return $CI;
  }

}

// if (!function_exists('sendEmail')) {
//   function sendEmail($slug, $email, $find, $replace) {
//     $CI = setProtocol();
//     $CI->load->model('email_template_model');
//     $res = $CI->email_template_model->getEmailTemplateBySlug($slug);
//     $msg = str_replace($find, $replace, $res->message);
//     $CI->email->from(EMAIL_FROM, FROM_NAME);
//     $CI->email->subject($res->subject);
//     $CI->email->message($msg);
//     $CI->email->to($email);
//     return $CI->email->send();
//   }
// }

if (!function_exists('sendEmail')) {
  function sendEmail($to, $subject, $msg, $reply_to = array()) {
    $CI = setProtocol();
    $CI->email->from(EMAIL_FROM, FROM_NAME);
    $CI->email->subject($subject);
    $CI->email->message($msg);
    $CI->email->to($to);
    if(!empty($reply_to) && is_array($reply_to)){
      $CI->email->reply_to($reply_to[0], $reply_to[1]);
    }
    return $CI->email->send();
  }
}

function filterImage($imageWithPath = '',$subDir = ''){
  if($subDir!=''){
    $rootPath = getThubmDir($imageWithPath);
    $mainImage = basename($imageWithPath);
    $imageUrl = $rootPath.'/'.$subDir.'/'.$mainImage;
    if(file_exists($imageUrl)){
      return base_url($imageUrl);
    }
    else{
      return base_url($imageWithPath);
    }
  }
  return $imageWithPath;
}
function removeImage($table,$filed,$id,$pathArray = array()){
  $helper = get_instance();
  $helper->load->model("model_common");
  $oldfile_name = $helper->model_common->imageExist($table,array($filed),array("id"=>$id));
  if(isset($oldfile_name->$filed) && $oldfile_name->$filed!="" && file_exists($oldfile_name->$filed)){
    unlink($oldfile_name->image);
    if(count($pathArray) > 0){
      $rootPath = getThubmDir($oldfile_name->$filed);
      $mainImage = basename($oldfile_name->image);
      foreach($pathArray as $path){
        unlink($rootPath.'/'.$path.'/'.$mainImage);
      }
    }
  }
}
function getThubmDir($thumbdir){
  $lastSection = basename($thumbdir);
  return str_replace('/'.$lastSection,'',$thumbdir);
}
function resize_image($filedir,$thumbdir,$new_width,$new_height){
  $path = getThubmDir($thumbdir);
  if (!is_dir($path)){
    mkdir($path, 0755, true);
  }
	$size=1600;
	$filedir =$filedir; // the directory for the original image
	$thumbdir =$thumbdir; // the directory for the thumbnail image
	$prefix = 'small_'; // the prefix to be added to the original name
	$mode = '0666';
	$prod_img = $filedir;
	$imageFileType = pathinfo($prod_img,PATHINFO_EXTENSION);
		$prod_img_thumb = $thumbdir;
		chmod ($prod_img, octdec($mode));
		$sizes = getimagesize($prod_img);
		$aspect_ratio = $sizes[1]/$sizes[0];
		if ($sizes[0] >= $size)
	 {
		 $new_height =$new_height;
		 $new_width =$new_width;
	 }
	 else
	 {
		$new_height =$new_height;
		$new_width =$new_width;
	}

	$destimg=ImageCreateTrueColor($new_width,$new_height) or die('Problem In Creating image');
	if($imageFileType=='png'||$imageFileType=='PNG')
	{
		$srcimg=ImageCreateFromPNG($prod_img) or die('Problem In opening Source Image');
        imagealphablending($destimg, false);
        imagesavealpha($destimg,true);
        $transparent = imagecolorallocatealpha($destimg, 255, 255, 255, 127);
        imagefilledrectangle($destimg, 0, 0, $new_width, $new_height, $transparent);
	}
	else if($imageFileType=='jpeg'||$imageFileType=='JPEG')
	{
		$srcimg=ImageCreateFromJPEG($prod_img) or die('Problem In opening Source Image');
	}
	else if($imageFileType=='jpg'||$imageFileType=='JPG')
	{
		$srcimg=ImageCreateFromJPEG($prod_img) or die('Problem In opening Source Image');
	}
	else if($imageFileType=='gif'||$imageFileType=='GIF')
	{
		$srcimg=ImageCreateFromGIF($prod_img) or die('Problem In opening Source Image');
	}
	if(function_exists('imagecopyresampled'))
	{
		imagecopyresampled($destimg,$srcimg,0,0,0,0,$new_width,$new_height,ImageSX($srcimg),ImageSY($srcimg)) or die('Problem In resizing');
	}
	else
	{
		Imagecopyresized($destimg,$srcimg,0,0,0,0,$new_width,$new_height,ImageSX($srcimg),ImageSY($srcimg)) or die('Problem In resizing');
	}
    if(strtolower($imageFileType)=='png'){
        imagepng($destimg,$prod_img_thumb)
        or die('Problem In saving');
    }
    else{
        ImageJPEG($destimg,$prod_img_thumb,90)
        or die('Problem In saving');
    }
	imagedestroy($destimg);
}

function make_url($string) {
  $string = strtolower(trim($string));
  $allowed = "/^[a-zA-Z0-9]+$/";
  if(!preg_match($allowed,$string)){
    $string = preg_replace($allowed,"",$string);
  }
  $string = str_replace(" ","_",$string);
  $string = str_replace("/","_",$string);
  if(strpos($string, "'")){
    $string = str_replace("'","+",$string);
  }
  $string = addslashes($string);
  return $string;
}

function is_listing_sold($property_sold) {
  // If property is sold, db has "yes"
  if ($property_sold == 'yes') {
    $bool = true;
  } else { // not sold
    $bool = false;
  }
  return $bool;
}

function is_listing_budget($payment_amount) {
  // If the payment was $25.00 or less it is a "budget" listing
  if (intval($payment_amount) <= 25) {
    $bool = true;
  } else { // not budget
    $bool = false;
  }
  return $bool;
}

function is_listing_expired($property_expired) {
  // if listing is 'expired' show expired view
  $today = date('Y-m-d H:i:s');
  // expired is set as datetime YYYY-MM-DD 00:00:00, so string compare
  if ($property_expired != NULL && $property_expired != '' && ($property_expired < $today)) {
    $bool = true;
  } else {
    $bool = false;
  }
  return $bool;
}

 function is_listing_deleted($property_deleted) {
  // if listing is 'deleted' show deleted view
  $today = date('Y-m-d');
  // deleted is set as date YYYY-MM-DD, so string compare
  if ($property_deleted != NULL && $property_deleted != '' && ($property_deleted <= $today)) {
  $bool = true;
  } else {
  $bool = false;
  }
  return $bool;
}

function cropString($string,$limit) {
  if(strlen($string) <= $limit) return $string;
  $string = SanitizeFromWord($string);
  $string = substr($string, 0,$limit);
  $rev_string = strrev($string);
  $pos = strpos($rev_string, ' ');
  $rev_string = "... ".substr($rev_string, $pos);
  return strrev($rev_string);
  }

function SanitizeFromWord($str = '') {
$chars = str_split($str);
foreach($chars as $key => $item){
if(in_array(ord($item),array(96,145,146))){ //Single Quotes
$chars[$key] = "'";
}
if(in_array(ord($item),array(147,148))){ // Double Quotes
$chars[$key] = '"';
}
if(in_array(ord($item),array(150,151))){ // Dashes
$chars[$key] = '-';
}
if(ord($item) == 133) $chars[$key] = '...';
if(ord($item) == 169) $chars[$key] = '&copy;';
if(ord($item) == 174) $chars[$key] = '&reg;';
}
return implode("",$chars);
}

function unslug($string) {
  $string = ucwords(strtolower($string));
  $string = str_replace("-"," ", stripslashes($string));
  $string = str_replace("+","'", stripslashes($string));
  if(strpos($string, "   ")){
    $string = str_replace("   "," - ", stripslashes($string));
  }
  return $string;
}

function generatePassword($length=9, $strength=0) {
$vowels = 'aeuy';
$consonants = 'bdghjmnpqrstvz';
if ($strength >= 1) {
  $consonants .= 'DGHJLMNPQRSTVWXZ';
}
if ($strength >= 2) {
  $vowels .= "AEUY";
}
if ($strength >= 3) {
  $consonants .= '23456789';
}
if ($strength >= 4) {
  $consonants .= '@#$!';
}

$password = '';
$alt = time() % 2;
for ($i = 0; $i < $length; $i++) {
  if ($alt == 1) {
    $password .= $consonants[(rand() % strlen($consonants))];
    $alt = 0;
  } else {
    $password .= $vowels[(rand() % strlen($vowels))];
    $alt = 1;
  }
}
return $password;
}

function uuid() {
  return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
  mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
  mt_rand( 0, 0x0fff ) | 0x4000,
  mt_rand( 0, 0x3fff ) | 0x8000,
  mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
}

function stateWiseProperty(){
  $helper = get_instance();
  $helper->load->model("common_model");
  return $helper->common_model->stateWiseProperty();
}

function countryWiseProperty(){
  $helper = get_instance();
  $helper->load->model("common_model");
  return $helper->common_model->countryWiseProperty();
}

function getMonthCount($date1, $date2){
  $ts1 = strtotime($date1);
  $ts2 = strtotime($date2);

  $year1 = date('Y', $ts1);
  $year2 = date('Y', $ts2);

  $month1 = date('m', $ts1);
  $month2 = date('m', $ts2);

  return $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
}

function getAlreadyApplyLeaveInfo($leaves=array(),$date){
  $leaves  = json_decode($leaves);
  $string = '';
  if(count($leaves) > 0){
    foreach($leaves as $leave){
      foreach(json_decode($leave->leave_detail) as $item){
        if($item->is_approved == 1 && $item->date == $date){
          $string .= "You have already applied for ".$date;
        }
      }
    }
  }
  return $string;
}

function getLeaveDetails($leave_id = 0){
  if($leave_id > 0){
    $leave = App\Leave::leftJoin('employees', function($join){
      $join->on('leaves.employee_id', '=', 'employees.id');
    })
    ->where('leaves.id',$leave_id)
    ->first(['leaves.*','employees.name']);
    if(!empty($leave->leave_detail)){
      $leave->leave_detail = json_decode($leave->leave_detail);
    }
    return $leave;
  }
  return false;
}

function getActiveEmails(){
  $emails = [];
  $employees = App\Employee::where('status','=','active')->get(['email']);
  if(count($employees) > 0){
    foreach($employees as $employee){
      $emails[] = $employee->email;
    }
  }
  return $emails;
}

?>
