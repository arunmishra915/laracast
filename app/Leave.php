<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model{
  protected $fillable = ['employee_id','from_date','to_date','leave_detail','have_leave_balance','leave_count','leave_status','reason'];
}
