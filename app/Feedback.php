<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model{
  protected $fillable = ['employee_id','message_from','message','is_read'];
}
