<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model{
  protected $fillable = ['employee_id','created_by_id','year','month','date_from','date_to','salary_per_month','paid_leaves','unpaid_leaves','bonus_amount','working_days','basic','hra','conveyance','medical_allowance','provident_fund','professional_tax','unpaid_leaves_amount','net_salary','total_earning','total_deduction'];
}
