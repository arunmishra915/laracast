<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model{
  protected $fillable = ['name','designation','joining_date','contact_number','emergency_contact_number','dob','bank_account','bank_name','ifcs_code','pancard','leave_balance','salary','document_available','document_returned','status','image','email','auth_id','password','role','document_returned_date'];

  public function processlogin($email = ""){
    if($email!=""){
      return DB::table('employees')->where('email','=',$email)->first();
    }
  }
}
