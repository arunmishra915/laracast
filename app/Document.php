<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model{
  protected $fillable = ['employee_id','document','document_discription'];
}
