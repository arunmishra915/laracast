(function(){
  $("#productForm").validate({
      errorClass: "text-danger",
      errorElement: "p",
      errorPlacement:function(error, element) {
        par=$(element);
        error.insertAfter(par);
      },
      rules:{
        email:{
          email:true
        },
        phone:{
          number:true,
        },
        confirm_password: {
            required: true,
            minlength: 5,
            equalTo: "#new_password"
        }
      }
    });

  var form_options = {
    dataType: "json",
    beforeSubmit: function(formData, jqForm, options){
      var formID = jqForm.attr('id');
      processing_Attr(formID);
    },
    success: function(json, responseText, xhr, form){
      var formID = form.attr('id');
      if(json.status == 1){
        if($(".productButton").length > 0){
          $(".itemName").empty().trigger('change');
        }
        successing_Attr(formID,json.message);
        if($(".productButton").length > 0){
          setTimeout(function(){
          $("#"+formID+" .alert").remove();
          $("#myModal").modal('toggle');
          },2000);
        }
        if(json.refresh){
            location.reload();
        }
        if(json.redirect){
        setTimeout(function(){
        location.assign(json.redirect);
        },2000);
        }
      }
      else{
        error_Attr(formID,json.message);
      }
      $("#"+formID)[0].reset();
    }
  };
  $("#productForm").ajaxForm(form_options);
})();

$(".productButton").click(function(){
  var queryType = $(this).text();
  var productTitle = $(this).parent().attr('product-name');
  $("#query_type").val(queryType);
  $("#product_name").val(productTitle);
  $('#myModal .modal-title').text(productTitle+': '+queryType);
});

  $('.itemName').select2({
  placeholder: 'Search Your Country',
  ajax: {
  url: baseUrl+'/home/query/getCountry',
  dataType: 'json',
  delay: 250,
  processResults: function (data) {
  return {
  results: data
  };
  },
  cache: true
  }
  });

  $(".action-on-stars li").click(function(){
    $(".action-on-stars li i").removeClass('fas');
    $(this).prevAll().find('i').addClass('fas');
    $(this).find('i').addClass('fas');
    var data = {}
    data['star_count'] = ($(this).index()+1);
    data['product_id'] = $(this).parent('.action-on-stars').attr('product-id');
    var oldRating = $(this).parent('.action-on-stars').attr('avg-rating');
    $.ajax({
      url:baseUrl+'home/query/productRating',
      method:'POST',
      dataType:'JSON',
      data:data,
      beforeSend: function(){
        console.log('processing...');
      },
      success: function(json){
        if(json.status == 1){
          $(".action-on-stars li i").removeClass('fas');
          $(".action-on-stars li:nth-child("+(json.avgRating)+")" ).prevAll().find('i').addClass('fas');
          $(".action-on-stars li:nth-child("+(json.avgRating)+")" ).find('i').addClass('fas');
          alert(json.message);
        }
        else{
          alert(json.message);
          $(".action-on-stars li i").removeClass('fas');
          $(".action-on-stars li:nth-child("+(oldRating)+")" ).prevAll().find('i').addClass('fas');
          $(".action-on-stars li:nth-child("+(oldRating)+")" ).find('i').addClass('fas');
        }
      }
    });
  });
