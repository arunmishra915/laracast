jQuery(document).ready(function() {
      console.log(url);
        var count = -1;
        table = jQuery('#email_template').DataTable({
          "bStateSave": true,
           "fnStateSave": function (oSettings, oData) {
               localStorage.setItem('offersDataTables', JSON.stringify(oData));
           },
           "fnStateLoad": function (oSettings) {
               return JSON.parse(localStorage.getItem('offersDataTables'));
           },
            "processing": true,
            "serverSide": true,
           "iDisplayLength": 10,
            "ajax": {
                url: url+"get_email_templates",
                type: "POST"
            },
            "columns": [
                { "data": "id" },
                { "data": "template_name" },
                { "data": "email_type" },
                { "data": "created_date" },
                { "data": "modified_date" },
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return "<button class='btn btn-info edit-email-template' data-id='"+row.id+"'  data-href='get_email_tempalte_by_id'  data-action='view'>View</button>&nbsp;&nbsp;<button class='btn btn-primary edit-email-template' data-id='"+row.id+"' data-href='get_email_tempalte_by_id' data-action='edit'>Edit</button>";
                    },
                    "targets": 5
                },
            ],
        });
        //var table = jQuery('#email_template').DataTable();
        $(document).on("click", ".edit-email-template", function(e){
            var self = $(this);
            $.ajax({
                url      : url+$(this).attr('data-href'),
                type     : "POST",
                datatype : 'json',
                data : {
                    id : $(this).attr('data-id')
                },
                beforeSend : function() {
                    $("#loading").show();
                },
                success : function(data) {
                    if(self.attr('data-action') == "edit") {
                        $("#template_id").val(data.id);
                        $("#template_name").val(data.template_name);
                        $("#subject").val(data.subject);
                        $("#comment").val(data.comment);
                        $("#type").val(data.email_type);
                        $("#template_content").val(data.template_content);
                        $('#template_content').wysihtml5()
                        $("#add-template-header").html("Edit email template")
                        $("#save").html("Update")
                        $("#add-template").modal("show");
                    } else if(self.attr('data-action') == "view"){
                        $("#template_name_label").html(data.template_name);
                        $("#template_content_label").html(data.template_content);
                        $("#template_subject_label").html(data.subject);
                        $("#comment_label").html(data.comment);
                        $("#type_label").html(data.type);
                        $("#view-template").modal("show");
                    }
                    $("#loading").hide();
                },
                error: function() {
                    $("#loading").hide();
                    alert("There is some error on server");
                },
                complete : function() {
                     $("#loading").hide();
                }
            });
        });

        $("#save_email_template").validate({
            errorClass: "text-danger",
            errorElement: "p",
            errorPlacement:function(error, element) {
              par=$(element);
              error.insertAfter(par);
            },
            rules:{
              email:{
                email:true
              },
              phone:{
                number:true,
              },
              confirm_password: {
                  required: true,
                  minlength: 5,
                  equalTo: "#new_password"
              }
            }
          });

        var form_options = {
          dataType: "json",
          beforeSubmit: function(formData, jqForm, options){
            var formID = jqForm.attr('id');
            processing_Attr(formID);
          },
          success: function(json, responseText, xhr, form){
            var formID = form.attr('id');
            if(json.status == 1){
              table.draw();
              successing_Attr(formID,json.message);
              $("#add-template").modal('hide');
              if(json.refresh){
                  location.reload();
              }
              if(json.redirect){
              setTimeout(function(){
              location.assign(json.redirect);
              },2000);
              }
            }
            else{
              error_Attr(formID,json.message);
            }
            $("#"+formID)[0].reset();
          }
        };
        $("#save_email_template").ajaxForm(form_options);
    });
