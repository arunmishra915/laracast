var isTest = function(value){
  if(value.toLowerCase() == 'yes'){
    $(".custom-test-field").removeAttr('disabled');
    $(".custom-test-field").parent().parent().show();
  }
  else{
    $(".custom-test-field").attr('disabled','disabled');
    $(".custom-test-field").parent().parent().hide();
  }
}
