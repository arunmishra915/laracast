var token = '';
if(localStorage){
  token = localStorage.gn_token;
}

if(localStorage.gn_pickup_location_form_data!=undefined){
  localStorage.removeItem('gn_pickup_location_form_data');
}
$(".deleteImage").click(function(){
  var data = {}
  data['image_number'] = $(this).attr('data-image');
  var imageId =  'file-'+data['image_number'];
  $.ajax({
  url: base_url+'analysis/deleteUploadImage',
  dataType: 'json',
  data: data,
  type: 'post',
  beforeSend: function(){
    $("."+imageId).attr("src",base_url+'assets/front/images/image_loader.gif');
  },
  success: function (res) {
    console.log(res);
    if($.trim(res.status) == 1){
      $("."+imageId).attr("src",res.image_default);
      $("."+imageId).removeClass("imageUploadDone");
      $("#delete-"+imageId).hide();
    }
    else{
      $("."+imageId).attr("src",base_url+'assets/front/images/image_upload_failed.jpg');
      alert(res.message);
    }
  }
  });
});

var uploadImge = function(element){
  var imageId =  $(element).attr('id');
  var file_data = $(element).prop('files')[0];
  var form_data = new FormData();
  form_data.append('file', file_data);
  form_data.append('image_number', $(element).attr('data-image'));
  $.ajax({
  url: base_url+'analysis/processUploadImage',
  dataType: 'json',
  cache: false,
  contentType: false,
  processData: false,
  data: form_data,
  type: 'post',
  beforeSend: function(){
    $("."+imageId).attr("src",base_url+'assets/front/images/image_loader.gif');
  },
  success: function (res) {
    if($.trim(res.status) == 1){
      $("."+imageId).attr("src",res.image_path);
      $("."+imageId).addClass("imageUploadDone");
      $("#delete-"+imageId).show();
    }
    else{
      $("."+imageId).attr("src",base_url+'assets/front/images/image_upload_failed.jpg');
      alert(res.message);
    }
  }
  });
}

$(".nextPageButton").click(function(){
  var counter = 0;
  $(".uplod_img img").each(function(index, value){
    if($(value).hasClass('imageUploadDone')){
      counter++;
    }
  })
  if(counter == 0){
    $("#instrctionForImageUpload").modal('toggle');
  }
  else{
    var url = $(this).attr('data-url');
    location.assign(url);
  }
});

$(".modal-Pass").click(function(){
  var url = $(".nextPageButton").attr('data-url');
  location.assign(url);
});
$(".modal-Upload").click(function(){
  $("#instrctionForImageUpload").modal('toggle');
});
(function(){
  $.ajax({
  url: base_url+'analysis/getModelImages',
  dataType: 'json',
  cache: false,
  contentType: false,
  processData: false,
  //data: form_data,
  type: 'post',
  success: function (res) {
    if(res.images){
      var imagesArray = res.images;
      $.each(imagesArray, function(index, value){
        $(".file-"+value.image_number).attr("src",base_url+value.file);
        $(".file-"+value.image_number).addClass('imageUploadDone');
        $("#delete-file-"+value.image_number).show();
      })
    }
  }
  });
})()
var gn_pickup_location = localStorage.gn_pickup_location;
if(gn_pickup_location!=undefined && gn_pickup_location == 1){
  localStorage.removeItem('gn_pickup_location');
}
