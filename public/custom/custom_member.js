// $.noConflict();
console.log(url);
var site_url = window.location.href;
var memberId = site_url.substr(site_url.lastIndexOf('/') + 1);
var messageTable = "";
jQuery(document).ready(function() {
  var count = -1;
  jQuery('#email_template').dataTable({
    "processing": true,
    "serverSide": true,
    "iDisplayLength": 50,
    "aaSorting": [[0, "desc"]],
    "ajax": {
    "url": url+"get_members_property/"+memberId,
    "type": "POST"
  },
  "columns": [
    { "data": "property_id" },
    { "data": "city" },
    { "data": "state" },
    { "data": "country" },
    { "data": "times_viewed" },
    { "data": "start_date" },
    { "data": "exp_date" },
    { "data": "property_id_md5" },
    { "data": "seo_slug" },
    { "data": "is_listing_expired" },
    { "data": "is_listing_deleted" },
    { "data": "beginSubscription" },
    { "data": "property_live" },
    { "data": "property_sold" },
  ],
  "columnDefs": [
  {
    "render": function ( data, type, row ) {
    if(!row.is_listing_deleted) {
    var str =  "<a class='btn btn-xs btn-info' onmouseover='$(this).tooltip();' href='"+base_url+"account/property/"+row.property_id_md5+"' title='Edit Property Listing' target='_blank'><i class='ace-icon fa fa-pencil-square-o'></i></a>&nbsp;&nbsp;<a class='btn btn-xs btn-success' title='View this listing' onmouseover='$(this).tooltip();' href='"+base_url+"listing/view/"+row.seo_slug+"/' target='_blank'><i class='ace-icon fa fa-eye'></i></a>&nbsp;&nbsp;<a class='btn btn-xs btn-warning' onmouseover='$(this).tooltip();' href='"+base_url+"account/photos/"+row.property_id_md5+"' title='Edit or Add Property Photos' target='_blank'><i class='ace-icon fa fa-camera'></i></a>";
    if(row.beginSubscription) {
    str += "&nbsp;&nbsp;<a class='btn btn-xs btn-primary' onmouseover='$(this).tooltip();' title='Begin Subscription' href='"+base_url+"checkout/cart/"+row.property_id+"' target='_blank'><i class='ace-icon fa fa-credit-card '></i></a>";
    } else {
    str += "&nbsp;&nbsp;<a class='btn btn-xs btn-danger' onmouseover='$(this).tooltip();' title='End Subscription' href='"+base_url+"home/account/endsubscription/"+row.property_id+"' target='_blank'><i class='ace-icon fa fa-credit-card '></i></a>";
    }
    str += "&nbsp;&nbsp;<button class='btn btn-xs btn-danger' onmouseover='$(this).tooltip();' title='Delete Property' data-target='#deletemodal"+row.property_id+"' data-toggle='modal'><i class='ace-icon fa fa-trash-o '></i></button>&nbsp;&nbsp;<button class='btn btn-xs btn-info' onmouseover='$(this).tooltip();' title='Social Statistics' onclick='showStatistics("+'"'+row.property_id_md5+'"'+")' ><i class='ace-icon fa fa-bar-chart-o '></i></button>";

    str += '<div class="modal fade" id="deletemodal'+row.property_id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="myModalLabel">Property '+row.property_id+' Delete</h4></div><div class="modal-body"><form class="form-horizontal delete_property_form" method="post" action="'+base_url+'home/account/deleteproperty"><div class="control-group mb5"><label class="control-label" for="email">Deleted Reason</label><div class="controls"><textarea class="form-control" rows="3" placeholder="Delete Reason" id="delete_reason" name="delete_reason" required></textarea></div></div><input type="hidden" name="id" value="'+row.property_id+'"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="submit" class="btn btn-danger ml3">Delete</button></form></div><div class="modal-footer"></div></div></div></div>'
    } else {
    var str = "<button class='btn btn-xs btn-warning showDeletedInfo' onmouseover='$(this).tooltip();' title='Deleted' onclick='showDeletedInfo("+row.property_id+")'><i class='ace-icon fa fa-exclamation-circle'></i></button>";
    }
    return str;
    },
    "targets": 14
  },
  { "visible": false,  "targets": [ 7 ] },
  { "visible": false,  "targets": [ 8 ] },
  { "visible": false,  "targets": [ 9 ] },
  { "visible": false,  "targets": [ 10 ] },
  { "visible": false,  "targets": [ 11 ] },
  ],
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    if ( aData.is_listing_deleted){
      $('td', nRow).css('background-color', '#f2dede');
      $('td', nRow).css('border-color', '#eed3d7');
      $('td', nRow).css('color', '#b94a48');
    }
  }
  });

  jQuery('#order_history_table').dataTable({
    "processing": true,
    "serverSide": true,
    "iDisplayLength": 50,
    "order": [[ 0, "desc" ]],
    "ajax": {
    "url": url+"get_orders/"+memberId,
    "type": "POST"
  },
  "columns": [
    { "data": "payment_id" },
    { "data": "date_created" },
    { "data": "created" },
    { "data": "billing_name" },
    { "data": "payment_total" },
    { "data": "property_id" },
    { "data": "deleted_reason" },
    { "data": "payment_id_md5" },
  ],
  "columnDefs": [
  {
  "render": function ( data, type, row ) {
  var requestType = "";
  if(row.created) {
    requestType = "'t'";
  }
  return '<button class="btn btn-primary btn-xs" onmouseover="$(this).tooltip();" onClick="viewOrderDetail('+"'"+row.payment_id_md5+"'"+', '+requestType+');" title="View payment detail" ><i class="ace-icon fa fa-eye"></i></button>';
  },
  "targets": 8
  },
  { "visible": false,  "targets": [ 2 ] },
  { "visible": false,  "targets": [ 7 ] },
  ],
  });

  messageTable = jQuery('#message_table').dataTable({
    "processing": true,
    "serverSide": true,
    "iDisplayLength": 50,
    "order": [[ 5, "desc" ]],
    "ajax": {
      "url": url+"get_messages/"+memberId,
      "type": "POST"
    },
    "columns": [
      { "data": "message_id" },
      { "data": "viewed" },
      { "data": "title" },
      { "data": "message" },
      { "data": "sender_name" },
      { "data": "created" },
      { "data": "message_id_md5" }
    ],
    "columnDefs": [
    {
      "render": function ( data, type, row ) {
        return '<button class="btn btn-primary btn-xs" onmouseover="$(this).tooltip();" onClick="viewMessageDetail('+"'"+row.message_id_md5+"'"+');" title="View message" ><i class="ace-icon fa fa-eye"></i></button>&nbsp;&nbsp;<button class="btn btn-danger btn-xs" onmouseover="$(this).tooltip();" onClick="deleteMessage('+"'"+row.message_id+"'"+');" title="Delete message" ><i class="ace-icon fa fa-trash-o"></i></button>';
      },
      "targets": 7
    },
    {
      "render": function ( data, type, row ) {
        return '<img src="'+base_url+'assets/home/images/message_'+row.viewed.toLowerCase()+'.gif" />';
      },
      "targets": 1
    },
    { "visible": false,  "targets": [ 0 ] },
    { "visible": false,  "targets": [ 6 ] },
    ],
  });

  $( "#message" ).on('click', function(e) {
    e.preventDefault();
    $("#message-modal").modal("show");
  });

  $( "#order" ).on('click', function(e) {
    e.preventDefault();
    $("#order-modal").modal("show");
  });
});

function viewOrderDetail(order_id, type) {
  $("#receipt").find(".modal-body").html("");
  $("#loading_image").show();
  $.ajax({
    url : base_url+"home/account/receipt/"+order_id+"?r="+type,
    beforeSend : function(){
      $("#loading_image").show();
    },
    success : function(data){
      $("#receipt").find(".modal-body").html(data);
      $("#receipt").modal("show");
      $("#loading_image").hide();
    }
  });
}
function viewMessageDetail(message_id) {
  $("#message_detail").find(".modal-body").html("");
  $("#loading_image").show();
  $.ajax({
    url : base_url+"home/account/message/"+message_id,
    beforeSend : function(){
      $("#loading_image").show();
    },
    success : function(data){
      $("#message_detail").find(".modal-body").html(data);
      $("#message_detail").modal("show");
      $("#loading_image").hide();
      var table = jQuery('#message_table').DataTable();
      table.draw(false);
    }
  });
}
function deleteMessage(message_id) {
  $("#loading_image").show();
  $.ajax({
    url : base_url+"home/account/message_delete_all/",
    type : 'POST',
    data : {
      blck_message : message_id
    },
    beforeSend : function(){
      $("#loading_image").show();
    },
    success : function(data){
      $("#message-deleted").show();
      $(".ui-tooltip").hide();
      $("#loading_image").hide();
      $("#message-deleted").html('<div class="alert alert-success">'+data+'</div>')
      var table = jQuery('#message_table').DataTable();
      table.draw(false);
      setTimeout(function(){
      $("#message-deleted").hide(500);
      $("#message-deleted").html("");
      }, 1500)
    }
  });
}

function showDeletedInfo(property_id) {
  $("#loading_image").show();
  $.ajax({
      url  : url+"get_delete_reason",
      type : 'POST',
      data : {
        property_id : property_id
      },
      beforeSend : function(){
        $("#loading_image").show();
      },
      success : function(data){
        data = JSON.parse(data);
        $("#message_detail").find(".modal-body").html("Deleted Date : "+data.Deleted_date +"<br><br><br>"+ data.Deleted_reason);
        $("#message_detail").modal("show");
        $("#loading_image").hide();
      }
  });
}

function showStatistics(property_id, viewType){
  if(typeof(viewType) == "undefined") {
    viewType = "month";
  }
  $("#loading_image").show();
  $.ajax({
      url  : url+"soicalStatistics/"+viewType+"/"+property_id,
      type : 'POST',
      beforeSend : function(){
          $("#loading_image").show();
      },
      success : function(data){
          $("#stat_modal").find(".modal-body").html(data);
          $("#stat_modal").modal("show");
          oTable = jQuery('#messagesl').dataTable({
              "aLengthMenu": [[25, 50, 100], [25, 50, 100]],
              "iDisplayLength": 50,
              "aaSorting": [[0, "desc"]],
              "sPaginationType": "full_numbers"
          });
          $("#loading_image").hide();
      }
  });
}

var doEdit = function(member_id){
  $.ajax({
    url: base_url+'admin/user/useredit',
    method: 'post',
    data: {member_id:member_id},
    dataType: 'json',
    beforeSend: function(){

    },
    success: function(json){
      console.log(json);
      if(json.modalContent){
        $("#modal-edit-modal .modal-content").html(json.modalContent);
        $("#modal-edit-modal").modal('show');
      }
      if(json.status == 1){
      }
      else{
      }
    }
  });
}
