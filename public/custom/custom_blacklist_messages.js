jQuery(document).ready(function () {
    oTable = jQuery('#messages').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[4, "desc"]],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [5]}]
    });
});
$(function () {
    $("#deletebtn").click(function () {
        if ($(".case:checked").length > 0) {
            if (confirm("Are you sure you want to delete this message? This is permanent and not recoverable.")) {
                return true;
            } else {
                return false;
            }
        } else {
            alert("Please choose any checkbox to delete.");
            return false;
        }
    });
    // add multiple select / deselect functionality
    $("#selectall").click(function () {
        $('.case').attr('checked', this.checked);
    });
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function () {
        if ($(".case").length == $(".case:checked").length) {
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }
    });
});
