jQuery(document).ready(function() {
oTable = jQuery('#messages').dataTable({
"bJQueryUI": true,
"sPaginationType": "full_numbers",
"aaSorting": [[0, "desc"]],
"iDisplayLength":50
});
});
$(document).ready(function(){
  $(".show-unsubscribe-modal").click(function(event){
    $("#unsubcribe-myModalLabel").html($(this).attr("data-value"));
    $("#unsubcribe-form").attr('action', $(this).attr("data-href"));
    $("#unsubcribe-modal").modal("show");
    $.ajax({
      url  : $(this).attr("data-href"),
      type : "POST"
    });
  });

  $(".unsubscribe").click(function(event){
    $("#unsubcribe-form").submit();
  });

  $(".mark-as-sold").click(function(event){
    var name = $(this).attr("data-name");
    var str = name+" - I sold my home ID#: ";
    var listing_id = $(this).attr("data-id");
    var address = $(this).attr("data-address");
    str += listing_id;
    str += "<br>"+address
    $("#sold-label").html(str);
    $("#listing_id").html(listing_id);
    $("#list_id").val(listing_id);
    $("#sold-modal").modal("show");
  });

  $(".sold-property").click(function(event){
  $("#sold-form").submit();
  });

  $('.delete_property_form').validate();

  $('.markcls').change(function () {
        var chkva = $(this).val();
        if ($(this).is(':checked')) {
            if ($('#markpropertyassoldstr').val()) {
                var oldv = $('#markpropertyassoldstr').val();
                $('#markpropertyassoldstr').val(oldv + ',' + chkva);
            } else {
                $('#markpropertyassoldstr').val(chkva);
            }
        } else {
            var oldv = $('#markpropertyassoldstr').val();
            oldv = oldv.replace($(this).val(), "");
            var modifiedString = oldv.replace(/,\s*$/, '');
            oldv = modifiedString.replace(",,", ",");
            $('#markpropertyassoldstr').val(oldv);
        }
    });
    $(".target").submit(function () {
        var r = confirm("Are you sure you want to mark this listing as Sold?");
        if (r == true) {
            return true;
        } else {
            return false;
        }
    });
});

function filtercall(crt,typ) {
  var typnew = document.getElementById(typ).value;
  if (crt != '') {
    window.location.href = url+'propertylisting/' + crt+'?f='+typnew;
  } else {
    window.location.href = url+'propertylisting?f='+typnew;
  }
}
function filtercall1(crt, typ) {
  var typnew = document.getElementById(typ).value;
  if (typnew != '') {
    window.location.href = url+'propertylisting/' + typnew + '?f=' + crt;
  } else {
    window.location.href = url+'propertylisting?f=' + crt;
  }
}
