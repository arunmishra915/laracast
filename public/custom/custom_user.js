// $.noConflict();
console.log(url);
jQuery(document).ready(function() {
    var count = -1;
    jQuery('#member_list').dataTable({
        "processing": true,
        "serverSide": true,
       "iDisplayLength": 50,
        "ajax": {
            url: url+"get_all_members",
            type: "POST",
            data:function(data) {
             data.search_type = jQuery('#search_type').val();
             data.search = jQuery('#search').val();
          }
        },
        "columns": [
            { "data": "member_id" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "total_property" },
            { "data": "telephone" },
            { "data": "email" },
            { "data": "active" },
            { "data": "member_type" },
        ],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return '<a class="btn btn-link" data-id="'+row.member_id+'"  href="/admin/member_override/'+row.member_id+'" data-action="view" target="_blank">#'+row.total_property+'</a>';
                },
                "targets": 3,
                "orderable": false
            },
            {
                "render": function ( data, type, row ) {
                    return "<button class='btn btn-xs btn-info' onmouseover='$(this).tooltip();' onClick='doEdit("+row.member_id+");'  Title='Edit member detail'><i class='ace-icon fa fa-pencil-square-o'></i></button>&nbsp;&nbsp;<button class='btn btn-xs btn-danger' member-id = '"+row.member_id+"' onclick='userdelete(this)' onmouseover='$(this).tooltip();' title='Delete member'><i class='ace-icon fa fa-trash-o'></i></button>&nbsp;&nbsp;<a class='btn btn-xs btn-primary' href='"+url+"member_override/"+row.member_id+"' target='_blank' data-action='user' onmouseover='$(this).tooltip();' title='Become "+row.first_name+"'><i class='ace-icon fa fa-user'></i></a>&nbsp;&nbsp;<a class='btn btn-xs btn-success' href='"+url+"member_override_admin/"+row.member_id+"' target='_blank'  data-action='become_admin' onmouseover='$(this).tooltip();' title='Become "+row.first_name+" with Admin Rights'><i class='ace-icon fa fa-users'></i></a>";
                },
                "targets": 8
            },
        ],
    });
    var table = jQuery('#member_list').DataTable();
    jQuery('#search_button').click(function() {
        if(jQuery('#search_type').val() != "" && jQuery('#search').val() != "") {
            table.draw();
        } else {
            jQuery("#error_message").show();
            jQuery("#error_message").html('<div class="alert alert-danger">Please Select Search Type and Enter value</div>');
            setTimeout(function(){
                jQuery("#error_message").hide(500);
                jQuery("#error_message").html("");
            }, 1500)
        }
    });
    jQuery('#reset_search').click(function() {
        jQuery('#search_type').val("");
        jQuery('#search').val("");
        table.draw();
    });

    $('#modal-edit-modal').on('hidden.bs.modal', function () {
      table.draw();
    });
});

var doEdit = function(member_id){
  $.ajax({
    url: url+'useredit',
    method: 'post',
    data: {member_id:member_id},
    dataType: 'json',
    beforeSend: function(){

    },
    success: function(json){
      console.log(json);
      if(json.modalContent){
        $("#modal-edit-modal .modal-content").html(json.modalContent);
        $("#modal-edit-modal").modal('show');
      }
      if(json.status == 1){
      }
      else{
      }
    }
  });
}
var userdelete = function(element){
  if (confirm("Are You Sure You Want To Delete Member?")){
    var data = {}
    data['member_id'] = $(element).attr('member-id');
    if(data['member_id'] > 0){
      $.ajax({
        url: url+'userdelete',
        method: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function(){

        },
        success: function(json){
          if(json.status == 1){
            general_Attr('#modal-custom-message',1,json.message);
            $(element).parents('tr').remove();
          }
          else{
            general_Attr('#modal-custom-message',2,json.message);
          }
          $("#modal-remove-modal").modal('show');
        }
      });
    }
  }
  else{
    return false;
  }
}
