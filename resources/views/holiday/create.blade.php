@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Holiday</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <div class="form-group col-sm-3">
        <a href="#" class="btn btn-success plus"><i class="fa fa-plus"></i>&nbsp;Add</a>
        <a href="#" class="btn btn-warning minus"><i class="fa fa-minus"></i>&nbsp;Remove</a>
      </div>
      <div align="right">
        <a href="{{ route('employee.index') }}" class="btn btn-success">Back</a>
      </div>
      <form method="post" action="{{ route('holiday.store') }}" enctype="multipart/form-data" class="form">
        @csrf
      <div class="row documents-container">
        <div class="col-sm-12 document-child">
          <div class="form-group col-sm-3"><input required class="form-control" name="holiday[]" type="text" placeholder="Holiday Name"></div>
          <div class="form-group col-sm-3"><input required id="cal-0" class="form-control holidayDate" name="date[]" type="text" data-date-format="yyyy-mm-d" placeholder="Holiday Date" autocomplete="off"></div>
          <div class="form-group col-sm-4">
            <select class="form-control" name="type[]">
              <option value="1">Normal Holiday</option>
              <option value="2">Restricted Holiday</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group col-sm-12 text-center">
      <input class="btn btn-primary" name="add" type="submit" value="Submit">
      </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>Holiday</th>
            <th>Date</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$holidays->isEmpty())
           @foreach($holidays as $key=>$holiday)
           <tr>
             <td scope="row">{{ $key+1 }}</td>
             <td>{{ $holiday->holiday }}</td>
             <td>{{ date('d F Y', strtotime($holiday->date)) }}</td>
             <td>@if($holiday->type == 1) {{ 'Normal Holiday' }} @else {{ 'Restricted Holiday' }} @endif</td>
             <td>
               <form style="display:inline-block;" action="{{ route('holiday.destroy', $holiday->id) }}" method="post">
                 @csrf
                 @method('DELETE')
                 <button type="submit" class="btn btn-danger">Delete</button>
               </form>
             </td>
           </tr>
           @endforeach
          @else
            <tr role="row">
              <td colspan="5" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script type="text/javascript">
  $("#cal-0").datepicker({autoclose: true,orientation:"down"});
  $(".plus").click(function(){
    $(".documents-container").append($(".document-child:first").clone(false));
    $(".holidayDate").attr('id');
    $(".holidayDate").each(function(index, value){
      var elementId = 'index-'+index;
      $(value).attr('id', elementId);
      $("#"+elementId).datepicker({autoclose: true,orientation:"down"});
    });
  });
  $(".minus").click(function(){
    if($(".document-child:last").index() > 0){
      $(".document-child:last").remove();
    }
  });
</script>
@stop
