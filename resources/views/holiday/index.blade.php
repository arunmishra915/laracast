@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Holiday</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <div class="row">
        <div class="col-sm-6">
          <div class="box box-success">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">Holidays</h3>
              <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th>Sn</th>
                    <th>Holiday</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$normalHolidays->isEmpty())
                   @foreach($normalHolidays as $key=>$holiday)
                   <tr>
                     <td scope="row">{{ $key+1 }}</td>
                     <td>{{ $holiday->holiday }}</td>
                     <td>{{ date('d F Y', strtotime($holiday->date)) }}</td>
                   </tr>
                   @endforeach
                  @else
                    <tr role="row">
                      <td colspan="3" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-sm-6">
          <div class="box box-danger">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">Restricted Holidays</h3>
              <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th>Sn</th>
                    <th>Holiday</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$restrictedHolidays->isEmpty())
                   @foreach($restrictedHolidays as $key=>$holiday)
                   <tr>
                     <td scope="row">{{ $key+1 }}</td>
                     <td>{{ $holiday->holiday }}</td>
                     <td>{{ date('d F Y', strtotime($holiday->date)) }}</td>
                   </tr>
                   @endforeach
                  @else
                    <tr role="row">
                      <td colspan="3" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
