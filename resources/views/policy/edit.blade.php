
@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Policy</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div align="right">
      <a href="{{ url('policy') }}" class="btn btn-success">Back</a>
    </div>
    @include('flash::message')
    <form method="post" action="{{ route('policy.update') }}" enctype="multipart/form-data" class="form">
    @csrf
    <div class="form-group col-sm-12">
    <label for="Content">Content</label>
    <textarea name="content" rows="8" cols="80" class="form-control" id="Content">{{ $policy->content }}</textarea>
    </div>
    <div class="form-group col-sm-12">
    <div class="checkbox">
    <label>
    <input type="checkbox" name="send_notification" value="1">
    Do you want to send email notificatin to all employees
    </label>
    </div>
    </div>
    <div class="form-group col-sm-12 text-center">
    <input class="btn btn-primary" name="update" type="submit" value="Update">
    </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop

@section('pagejs')
  <script type="text/javascript">
  CKEDITOR.config.allowedContent = true;
  CKEDITOR.replace('Content')
  </script>
@stop
