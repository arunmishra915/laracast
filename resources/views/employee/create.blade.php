@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Employee</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @include('flash::message')
    <form method="post" action="{{ route('employee.store') }}" enctype="multipart/form-data" class="form">
    @csrf
    <div class="form-group col-sm-6">
    <label for="Name">Employee Name<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="name" value="{{ old('name') }}" type="text" id="Name" placeholder="Employee Name" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Designation">Designation<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="designation" value="{{ old('designation') }}"  type="text" id="Designation" placeholder="Designation" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Email">Employee Email<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="email" value="{{ old('email') }}" type="text" id="Email" placeholder="Employee Email" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Auth_Id">Employee Auth Id<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="auth_id" value="{{ old('auth_id') }}" type="text" id="Auth_Id" placeholder="Employee Auth Id" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Auth_Id">Password<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="password" type="text" id="Password" placeholder="Password" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Employee_Role">Employee Role<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <select class="form-control" name="role"  id="Employee_Role" required>
      <option value="1" {{ old('role') == '1' ? 'selected' : '' }}>Admin</option>
      <option value="2" {{ old('role') == '2' ? 'selected' : '' }}>HR</option>
      <option value="3" {{ old('role') == '3' ? 'selected' : '' }}>Employee</option>
    </select>
    </div>
    <div class="form-group col-sm-6">
    <label for="Contact_Number">Contact Number<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="contact_number" value="{{ old('contact_number') }}" type="text" id="Contact_Number" placeholder="Contact Number" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Emergency_Contact_Number">Emergency Contact Number</label>
    <input class="form-control" name="emergency_contact_number" value="{{ old('emergency_contact_number') }}" type="text" id="Emergency_Contact_Number" placeholder="Emergency Contact Number">
    </div>
    <div class="form-group col-sm-6">
    <label for="Date_of_Birth">Date of Birth<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="dob" value="{{ old('dob') }}" type="text" id="Date_of_Birth" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="Date of Birth" autocomplete="off" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Joining_Date">Joining Date<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="joining_date" value="{{ old('joining_date') }}" type="text" id="Joining_Date" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="Joining Date"  autocomplete="off" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Leave_Balance">Leave Balance</label>
    <input class="form-control" name="leave_balance" value="0" type="text" id="Leave_Balance"  placeholder="Leave Balance"  autocomplete="off">
    </div>
    <!-- <div class="form-group col-sm-6">
    <label for="Salary">Salary<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="salary" value="{{ old('salary') }}" type="text" id="Salary" placeholder="Salary" required>
    </div> -->
    <div class="form-group col-sm-6">
    <label for="Bank_Name">Bank Name<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="bank_name" value="{{ old('bank_name') }}" type="text" id="Bank_Name" placeholder="Bank Name" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Bank_Account">Bank Account<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="bank_account" value="{{ old('bank_account') }}" type="text" id="Bank_Account" placeholder="Bank Account" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="IFCS_Code">IFSC Code<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="ifcs_code" value="{{ old('ifcs_code') }}" type="text" id="IFCS_Code" placeholder="IFSC Code" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Pancard">Pancard<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="pancard" value="{{ old('pancard') }}" type="text" id="Pancard" placeholder="Pancard" required>
    </div>
    <div class="form-group col-sm-6">
    <label for="Documents_Available">Documents Available<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <select class="form-control" name="document_available"  id="Documents_Available" required>
      <option value="yes" {{ old('document_available') == 'yes' ? 'selected' : '' }} >Yes</option>
      <option value="no" {{ old('document_available') == 'no' ? 'selected' : '' }} >No</option>
    </select>
    </div>
    <div class="form-group col-sm-6">
    <label for="Documents_Returned">Documents Returned<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <select class="form-control" name="document_returned" id="Documents_Returned" required>
      <option value="not submitted" {{ old('document_returned') == 'not submitted' ? 'selected' : ''}} >Not Submitted</option>
      <option value="yes"  {{ old('document_returned') == 'yes' ? 'selected' : ''}} >Yes</option>
      <option value="no" {{ old('document_returned') == 'no' ? 'selected' : ''}} >No</option>
    </select>
    </div>
    <div class="form-group col-sm-6">
    <label for="status">Status<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <select class="form-control" name="status" id="status" required>
      <option value="active" {{ old('status') == 'active' ? 'selected' : '' }}>Active</option>
      <option value="left" {{ old('status') == 'left' ? 'selected' : '' }}>Left</option>
    </select>
    </div>
    <div class="form-group col-sm-6">
    <label for="image">Image</label>
    <input class="form-control" name="image" type="file" id="image">
    </div>
    <div class="form-group col-sm-12 text-center">
    <input class="btn btn-primary" name="add" type="submit" value="Add">
    </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
 $('#Date_of_Birth, #Joining_Date, #Anniversary_Date').datepicker({
    autoclose: true
  });
</script>
@stop
