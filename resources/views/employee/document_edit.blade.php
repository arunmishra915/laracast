@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Employee</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @endif
          <div align="right">
          <a href="{{ url('employee/document',$document->employee_id) }}" class="btn btn-success">Back</a>
          </div>
          <form method="post" action="{{ url('employee/document_update') }}" enctype="multipart/form-data" class="form">
            @csrf
          <div class="row documents-container">
            <div class="col-sm-12 document-child">
              <div class="form-group col-sm-6"><input required class="form-control" name="document_discription" value="{{ $document->document_discription }}" type="text" placeholder="Document Discription"></div>
              <div class="form-group col-sm-4"><input class="form-control" name="document_image" type="file"></div>
              <div class="form-group col-sm-2">
                <a href="{{ url('public/employee_documents',$document->document) }}" target="_blank" title="View Document"><i class="fa fa-file fa-2x"></i></a>
              </div>
            </div>
          </div>
          <div class="form-group col-sm-12 text-center">
          <input type="hidden" name="old_document" value="{{ $document->document }}">
          <input type="hidden" name="employee_id" value="{{ $document->employee_id }}">
          <input type="hidden" name="id" value="{{ $document->id }}">
          <input class="btn btn-primary" name="add" type="submit" value="Submit">
          </div>
        </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
 $('#Date_of_Birth, #Joining_Date, #Anniversary_Date').datepicker({
    autoclose: true
  });
  $(".plus").click(function(){
    $(".documents-container").append($(".document-child:first").clone(true));
  });
  $(".minus").click(function(){
    if($(this).parent().parent().index() > 0){
      $(this).parent().parent().remove();
    }
  });
</script>
@stop
