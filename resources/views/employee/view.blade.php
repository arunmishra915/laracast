@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Employee</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div class="row">

      </div>
      <div class="row">
        <div class="col-xs-6">
          <table class="table table-bordered table-hover" role="grid">
            <tr>
              <th>Name</th><td>{{ $employee->name }}</td>
            </tr>
            <tr>
              <th>Employee Id</th><td>{{ $employee->auth_id }}</td>
            </tr>
            <tr>
              <th>Designation</th><td>{{ $employee->designation }}</td>
            </tr>
            <tr>
              <th>Email</th><td>{{ $employee->email }}</td>
            </tr>
            <tr>
              <th>Contact Number</th><td>{{ $employee->contact_number }}</td>
            </tr>
            <tr>
              <th>Emergency Contact Number</th><td>{{ $employee->emergency_contact_number }}</td>
            </tr>
            <tr>
              <th>Date of Birth</th><td>{{ date('d F Y', strtotime($employee->dob)) }}</td>
            </tr>
            <tr>
              <th>Joining Date</th><td>{{ date('d F Y', strtotime($employee->joining_date)) }}</td>
            </tr>
            <tr>
              <th>Leave Balance</th><td>{{ $employee->leave_balance }}</td>
            </tr>
            <tr>
              <th>Bank Name</th><td>{{ $employee->bank_name }}</td>
            </tr>
            <tr>
              <th>Bank Account</th><td>{{ $employee->bank_account }}</td>
            </tr>
            <tr>
              <th>IFSC Code</th><td>{{ $employee->ifcs_code }}</td>
            </tr>
            <tr>
              <th>Pancard</th><td>{{ $employee->pancard }}</td>
            </tr>
            <tr>
              <th>Status</th><td>{{ ucwords($employee->status) }}</td>
            </tr>
            <tr>
              <th>Documents Available</th><td>{{ ucwords($employee->document_available) }}</td>
            </tr>
            <tr>
              <th>Documents Returned</th><td>{{ ucwords($employee->document_returned) }}</td>
            </tr>
            @if($employee->document_returned == 'yes')
            <tr>
              <th>Documents Returned Date</th><td>{{ getDateTime($employee->document_returned_date,'d F Y') }}</td>
            </tr>
            @endif
          </table>
        </div>
        <div class="col-xs-6">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="@if($employee->image!='') {{ url('public/profile/thumbnail/'.$employee->image ) }} @else {{ url('public/profile/dummy-profile-image.jpg') }}  @endif" alt="User profile picture">
              <h3 class="profile-username text-center">{{ $employee->name }}</h3>
              <p class="text-muted text-center">{{ $employee->designation }}</p>
              <a href="{{ route('employee.edit',$employee->id) }}" class="btn btn-primary btn-block"><b>Edit Profile</b></a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
