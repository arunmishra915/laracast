@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div align="right">
        <a href="{{ route('employee.create') }}" class="btn btn-success">Add</a>
      </div>
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Joining Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$employees->isEmpty())
            @foreach($employees as $key=>$employee)
            <tr>
              <td scope="row">{{ $employees->firstItem()+$key }}</td>
              <td>{{ $employee->name }}</td>
              <td>{{ $employee->designation }}</td>
              <td>{{ date('d F Y', strtotime($employee->joining_date)) }}</td>
              <td>
                <a class="btn btn-primary" href="{{ route('employee.show',$employee->id) }}">Show</a>
                <a class="btn btn-warning" href="{{ route('employee.edit',$employee->id) }}">Edit</a>
                <form style="display:inline-block;" action="{{ route('employee.destroy', $employee->id) }}" method="post">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-danger"  onclick="return confirm('Are you sure?')">Delete</button>
                </form>
                <!-- <a class="btn btn-success" href="{{ route('salary.overview',$employee->id) }}">Payroll</a> -->
                <a class="btn btn-success" href="{{ route('payroll.create',$employee->id) }}">Payroll</a>
                @if(in_array($user->role,array(1)))
                <a class="btn btn-default" href="{{ route('salary.view',$employee->id) }}">View Salary</a>
                @endif
                @if($employee->document_available == 'yes')
                <a class="btn btn-info" href="{{ url('employee/document/'.$employee->id) }}">Documentation</a>
                @endif
              </td>
            </tr>
            @endforeach
          @else
            <tr role="row">
              <td colspan="5" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
      {!! $employees->links() !!}
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
