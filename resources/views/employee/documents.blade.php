@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Employee</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <div align="right">
        <a href="{{ route('employee.index') }}" class="btn btn-success">Back</a>
      </div>
      <form method="post" action="{{ url('employee/documentupload') }}" enctype="multipart/form-data" class="form">
        @csrf
      <div class="row documents-container">
        <div class="col-sm-12 document-child">
          <div class="form-group col-sm-6"><input required class="form-control" name="document_discription[]" type="text" placeholder="Document Discription"></div>
          <div class="form-group col-sm-4"><input required class="form-control" name="document_image[]" type="file"></div>
          <div class="form-group col-sm-2">
            <a href="#" class="btn btn-success plus"><i class="fa fa-plus"></i></a>
            <a href="#" class="btn btn-warning minus"><i class="fa fa-minus"></i></a>
          </div>
        </div>
      </div>
      <div class="form-group col-sm-12 text-center">
      <input type="hidden" name="employee_id" value="{{ $data['employee_id'] }}">
      <input class="btn btn-primary" name="add" type="submit" value="Submit">
      </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-bordered table-hover" role="grid">
        <thead>
          <tr role="row">
            <th>Documnet Discription</th>
            <th>Document File</th>
            <th>Activity</th>
          </tr>
        </thead>
        <tbody>
          @if(!$data['documents']->isEmpty())
            @foreach($data['documents'] as $document)
              <tr role="row">
                <td>{{ $document['document_discription'] }}</td>
                <td> <a href="{{ url('public/employee_documents',$document->document) }}" target="_blank" title="View Document"><i class="fa fa-file fa-2x"></i></a></td>
                <td>
                  <a class="btn btn-warning" href="{{ url('employee/document_edit',$document['id']) }}">Edit</a>
                  <a class="btn btn-danger" href="{{ url('employee/document_destroy',$document['id']) }}">Delete</a>
                </td>
              </tr>
            @endforeach
          @else
            <tr role="row">
              <td colspan="3" class="alert alert-warning"><strong>Sorry</strong> , documents are not available</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  $(".plus").click(function(){
    $(".documents-container").append($(".document-child:first").clone(true));
  });
  $(".minus").click(function(){
    if($(this).parent().parent().index() > 0){
      $(this).parent().parent().remove();
    }
  });
</script>
@stop
