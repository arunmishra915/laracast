<section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="active"><a href="{{ url('dashboard') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
    @if(in_array($user->role,array(1,2)))
    <li class="treeview {{ $className == '\EmployeeController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Employee</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    <li class="{{ $methodName == 'create'?'active':'' }}"><a href="{{ route('employee.create') }}"><i class="fa fa-circle-o"></i> Add Employee</a></li>
    <li  class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('employee.index') }}"><i class="fa fa-circle-o"></i> Manage Employee</a></li>
    </ul>
    </li>
    @endif
    <li class="treeview  {{ $className == '\LeaveController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Leave</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    <li class="{{ $methodName == 'create'?'active':'' }}"><a href="{{ route('leave.create') }}"><i class="fa fa-circle-o"></i> Apply Leave</a></li>
    <li  class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('leave.view') }}"><i class="fa fa-circle-o"></i> My Leaves</a></li>
    @if(in_array($user->role,array(1,2)))
    <li  class="{{ $methodName == 'leaverequests'?'active':'' }}"><a href="{{ route('leave.leaverequests') }}"><i class="fa fa-circle-o"></i> Approve/Unapprove</a></li>
    @endif
    </ul>
    </li>
    <li class="treeview  {{ $className == '\PolicyController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Policy</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    @if(in_array($user->role,array(1,2)))
      <li class="{{ $methodName == 'edit'?'active':'' }}"><a href="{{ route('policy.edit') }}"><i class="fa fa-circle-o"></i>  Edit Company Policy</a></li>
    @endif
    <li  class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('policy.index') }}"><i class="fa fa-circle-o"></i> Company Policy</a></li>
    </ul>
    </li>
    <li class="treeview  {{ $className == '\HolidayController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Holiday</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    @if(in_array($user->role,array(1,2)))
      <li class="{{ $methodName == 'edit'?'active':'' }}"><a href="{{ route('holiday.create') }}"><i class="fa fa-circle-o"></i>  Add Holiday</a></li>
    @endif
    <li  class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('holiday.index') }}"><i class="fa fa-circle-o"></i> Holiday List</a></li>
    </ul>
    </li>
    <li class="treeview  {{ $className == '\SuggestionController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Feedback</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    @if(in_array($user->role,array(1,2)))
    <li class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('suggestion.index') }}"><i class="fa fa-circle-o"></i>  View Feedbacks</a></li>
    @else
    <li class="{{ $methodName == 'create'?'active':'' }}"><a href="{{ route('suggestion.create') }}"><i class="fa fa-circle-o"></i>  Send Feedback</a></li>
    @endif
    </ul>
    </li>
    <li class="treeview  {{ $className == '\SalaryController'?'active menu-open':''}}">
    <a href="#">
    <i class="fa fa-table"></i> <span>Salary</span>
    <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
    <ul class="treeview-menu">
    @if(in_array($user->role,array(1,2)))
    <li class="{{ $methodName == 'create'?'active':'' }}"><a href="{{ route('salary.create') }}"><i class="fa fa-circle-o"></i> Create Salary</a></li>
    @endif
    <li  class="{{ $methodName == 'index'?'active':'' }}"><a href="{{ route('salary.index') }}"><i class="fa fa-circle-o"></i> View Salary</a></li>
    </ul>
    </li>
    <li class=""><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> <span>Sign out</span></a></li>
    </ul>
</section>
