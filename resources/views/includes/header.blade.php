<a href="#" class="logo">
  <span class="logo-mini"><b>D</b>elimp</span>
  <span class="logo-lg"> <img src="{{asset('public/img/logo.png')}}"></span>
</a>
<nav class="navbar navbar-static-top">
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-bell-o"></i>
          @if(!$notifications->isEmpty())
            <span class="label label-warning">{{ count($notifications) }}</span>
          @endif
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have {{ count($notifications) }} {{ count($notifications) >1 ?'notifications':'notification' }}</li>
          <li>
            <!-- inner menu: contains the actual data -->
            @if(!$notifications->isEmpty())
            <ul class="menu">
              @foreach($notifications as $notification)
                <li>
                  <a href="{{ $notification->notification_url }}">
                    <i class="fa fa-users"></i> {{ $notification->message }}
                  </a>
                </li>
              @endforeach
            </ul>
            @endif
          </li>
        </ul>
      </li>

      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="@if(Session::get('user')->image!='' && file_exists('public/profile/thumbnail/'.Session::get('user')->image)) {{ url('public/profile/'.Session::get('user')->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }}  @endif" class="user-image" alt="User Image">
          <span class="hidden-xs">{{ Session::get('user')->name }}</span>
        </a>
        <ul class="dropdown-menu">
          <li class="user-header">
            <img src="@if(Session::get('user')->image!='' && file_exists('public/profile/thumbnail/'.Session::get('user')->image)) {{ url('public/profile/'.Session::get('user')->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }}  @endif" class="img-circle" alt="User Image">
            <p>{{ Session::get('user')->name }}</p>
          </li>
          <li class="user-footer">
            <div class="pull-right">
              <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
