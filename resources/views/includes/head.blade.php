<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Delimp | Dashboard</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
{{ HTML::style('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
{{ HTML::style('public/bower_components/font-awesome/css/font-awesome.min.css') }}
{{ HTML::style('public/bower_components/Ionicons/css/ionicons.min.css') }}
{{ HTML::style('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
{{ HTML::style('public/dist/css/AdminLTE.min.css') }}
{{ HTML::style('public/dist/css/skins/_all-skins.min.css') }}
{{ HTML::style('public/bower_components/morris.js/morris.css') }}
{{ HTML::style('public/bower_components/jvectormap/jquery-jvectormap.css') }}
{{ HTML::style('public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}
{{ HTML::style('public/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}
{{ HTML::style('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
{{ HTML::style('public/dist/css/custom-style.css') }}
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
