<footer class="main-footer">
  
</footer>
{{ HTML::script('public/bower_components/jquery/dist/jquery.min.js') }}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- jQuery UI 1.11.4 -->
{{ HTML::script('public/bower_components/jquery-ui/jquery-ui.min.js') }}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
{{ HTML::script('public/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
{{ HTML::script('public/bower_components/datatables.net/js/jquery.dataTables.min.js') }}
{{ HTML::script('public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}
{{ HTML::script('public/bower_components/raphael/raphael.min.js') }}
{{ HTML::script('public/bower_components/morris.js/morris.min.js') }}
{{ HTML::script('public/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}
{{ HTML::script('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
{{ HTML::script('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
{{ HTML::script('public/bower_components/jquery-knob/dist/jquery.knob.min.js') }}
{{ HTML::script('public/bower_components/moment/min/moment.min.js') }}
{{ HTML::script('public/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}
{{ HTML::script('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}
{{ HTML::script('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
{{ HTML::script('public/bower_components/ckeditor/ckeditor.js') }}
{{ HTML::script('public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}
{{ HTML::script('public/bower_components/fastclick/lib/fastclick.js') }}
{{ HTML::script('public/dist/js/adminlte.min.js') }}
{{ HTML::script('public/dist/js/pages/dashboard.js') }}
{{ HTML::script('public/dist/js/demo.js') }}
{{ HTML::script('public/custom/jquery.form.js') }}
{{ HTML::script('public/custom/jquery.validate.js') }}
{{ HTML::script('public/custom/custom-main.js') }}
