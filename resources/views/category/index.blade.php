@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>Dashboard</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Report</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div align="right">
        <a href="{{ route('category.create') }}" class="btn btn-success">Add</a>
      </div>
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th><th>Name</th><th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categories as $key=>$category)
          <tr>
            <td scope="row">{{ $key+1 }}</td><td>{{ $category->name }}</td>
            <td>
              <a class="btn btn-primary" href="{{ route('category.show',$category->id) }}">Show</a>
              <a class="btn btn-warning" href="{{ route('category.edit',$category->id) }}">Edit</a>
              <form style="display:inline-block;" action="{{ route('category.destroy', $category->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {!! $categories->links() !!}
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
