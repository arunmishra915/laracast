@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Feedback</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="box box-success">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">Messages</h3>
              <div class="chat-custom-container">
              @if(!$messages->isEmpty())
                @foreach($messages as $key=>$message)
                  @if($message->employee_id == $message->message_from)
                    <div class="chat-container darker" id="feedback-{{ $message->id }}{{ $message->employee_id }}" chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
                    <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar" class="right">
                    <p>{{ $message->message }}</p>
                    <span class="time-left"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;<i class="@if($message->is_read == 1) {{ 'fa fa-eye text-green' }} @else {{ 'fa fa-eye-slash' }} @endif" aria-hidden="true"></i>&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
                    </div>
                  @else
                    <div class="chat-container" id="feedback-{{ $message->id }}{{ $message->employee_id }}"  chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
                    <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar">
                    <p>{{ $message->message }}</p>
                    <span class="time-right"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
                    </div>
                  @endif
                @endforeach
              @endif
            </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-sm-12">
          <div class="box box-success">
            <div class="box-body box-profile">
              @include('flash::message')
              @if($message = Session::get('success'))
              <div class="alert alert-success">
                <p>{{ $message }}</p>
              </div>
              @endif
              <form method="post" action="{{ route('feedback.store') }}" enctype="multipart/form-data" class="form">
              @csrf
              <div class="form-group col-sm-12">
              <label for="Reason">Message</label>
              <input class="form-control" name="message" value="{{ old('message') }}" type="text" id="Reason" placeholder="Reason">
              </div>
              <div class="form-group col-sm-12 text-center">
              <input type="hidden" name="employee_id" value="{{ $user->id }}">
              <input class="btn btn-primary" name="add" type="submit" value="Send">
              <a href="{{ route('feedback.index') }}" class="btn btn-info">Refresh</a>
              </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script type="text/javascript">
  $(".chat-custom-container").animate({ scrollTop: $('.chat-custom-container').prop("scrollHeight")}, 1000);
  $(".chat-custom-container").bind("scroll", function() {
    if($(this).scrollTop() == 0){
      console.log("hit request");
      var feedback_id = $(".chat-container:first").attr('chat-id');
      var employee_id = $(".chat-container:first").attr('employee-id');
      getPreviousChat(feedback_id, employee_id);
    }
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });

  var getPreviousChat = function(feedback_id, employee_id){
    $.ajax({
      url:'{{ url('feedback/getPreviousChat')}}',
      method:'post',
      data:{feedback_id:feedback_id,employee_id:employee_id,is_admin:0},
      dataType:'json',
      beforeSend: function(){
        console.log("+++++Requesting+++++");
      },
      success: function(json){
        if(json.feedback_count > 0){
          $(".chat-custom-container").prepend(json.feedbacks);
          $('.chat-custom-container').scrollTop(50);
        }
      }
    })
  }
</script>
@stop
