@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div align="right">
        <a href="{{ route('employee.create') }}" class="btn btn-success">Add</a>
      </div>
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Joining Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$employees->isEmpty())
            @foreach($employees as $key=>$employee)
            <tr>
              <td scope="row">{{ $key+1 }}</td>
              <td>{{ $employee->name }}</td>
              <td>{{ $employee->designation }}</td>
              <td>{{ date('d F Y', strtotime($employee->joining_date)) }}</td>
              <td>
                <a class="btn btn-primary" href="{{ route('feedback.getfeedback',$employee->id) }}">Feedback</a>
              </td>
            </tr>
            @endforeach
          @else
            <tr role="row">
              <td colspan="5" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
