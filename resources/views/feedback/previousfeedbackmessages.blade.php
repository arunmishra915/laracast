@if(!$messages->isEmpty())
  @if($is_admin == 0)
    @foreach($messages as $key=>$message)
      @if($message->employee_id == $message->message_from)
        <div class="chat-container darker" id="feedback-{{ $message->id }}{{ $message->employee_id }}" chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
        <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar" class="right">
        <p>{{ $message->message }}</p>
        <span class="time-left"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;<i class="@if($message->is_read == 1) {{ 'fa fa-eye text-green' }} @else {{ 'fa fa-eye-slash' }} @endif" aria-hidden="true"></i>&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
        </div>
      @else
        <div class="chat-container" id="feedback-{{ $message->id }}{{ $message->employee_id }}"  chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
        <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar">
        <p>{{ $message->message }}</p>
        <span class="time-right"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
        </div>
      @endif
    @endforeach
  @else
  @foreach($messages as $key=>$message)
    @if($message->employee_id == $message->message_from)
      <div class="chat-container" id="feedback-{{ $message->id }}{{ $message->employee_id }}"  chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
      <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar">
      <p>{{ $message->message }}</p>
      <span class="time-right"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
      </div>
    @else
    <div class="chat-container darker" id="feedback-{{ $message->id }}{{ $message->employee_id }}" chat-id="{{ $message->id }}" employee-id="{{ $message->employee_id }}">
    <img src="@if($message->image!='' && file_exists('public/profile/'.$message->image)) {{ url('public/profile', $message->image) }} @else {{ url('public/profile/dummy-profile-image.jpg') }} @endif" alt="Avatar" class="right">
    <p>{{ $message->message }}</p>
    <span class="time-left"><strong>{{ $message->name }}</strong>&nbsp;&nbsp;<i class="@if($message->is_read == 1) {{ 'fa fa-eye text-green' }} @else {{ 'fa fa-eye-slash' }} @endif" aria-hidden="true"></i>&nbsp;{{ getDateTime($message->created_at,'d F Y h:i a') }}</span>
    </div>
    @endif
  @endforeach
  @endif

@endif
