@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{$pageHeading}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">{{$pageHeading}}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{$pageHeading}}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th><th>Employee</th><th>Subject</th><th>Date</th><th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($suggestions as $key=>$suggestion)
          <tr>
            <td scope="row">{{ $key+$suggestions->firstItem() }}</td>
            <td>{{ $suggestion->name }}</td>
            <td>{{ $suggestion->subject }}</td>
            <td>{{ getDateTime($suggestion->created_at, 'd F Y  h:s A') }}</td>
            <td>
              <a class="btn btn-primary" href="{{ route('suggestion.show',$suggestion->id) }}">Show</a>
              @if(in_array($user->role,array(1)))
              <form style="display:inline-block;" action="{{ route('suggestion.destroy', $suggestion->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
              </form>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {!! $suggestions->links() !!}
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
