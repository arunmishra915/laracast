@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Feedback</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
    <form method="post" action="{{ route('suggestion.store') }}" enctype="multipart/form-data" class="form">
    @csrf
    <div class="form-group col-sm-6">
    <label for="Subject">Subject<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <input class="form-control" name="subject" value="{{ old('subject') }}" type="text" id="Subject" placeholder="Subject" required>
    </div>
    <div class="form-group col-sm-12">
    <label for="Description">Description<i class="fa fa-star text-red" aria-hidden="true"></i></label>
    <textarea class="form-control" name="description" id="Description" placeholder="Description" cols="30" rows="10">{{ old('description') }}</textarea>
    </div>
    <div class="form-group col-sm-12 text-center">
    <input class="btn btn-primary" name="add" type="submit" value="Add">
    </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
 $('#Date_of_Birth, #Joining_Date, #Anniversary_Date').datepicker({
    autoclose: true
  });
</script>
@stop
