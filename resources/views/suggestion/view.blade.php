@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{$pageHeading}}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Dashboard</a></li>
      <li class="active">{{$pageHeading}}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{$pageHeading}}</h3>

              <div class="box-tools pull-right">
                <a href="{{ route('suggestion.index') }}" class="btn btn-success" title=""><i class="fa fa-chevron-left"></i>Back</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>{{$suggestion->subject}}</h3>
                <h5>From: {{$suggestion->email}}
                  <span class="mailbox-read-time pull-right">15 Feb. 2016 11:03 PM</span></h5>
              </div>
              <div class="mailbox-read-message custom-message">
                  <pre>{{$suggestion->description}}</pre>
              </div>
            </div>
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
