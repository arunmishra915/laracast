<!DOCTYPE html>
<html>
<head>
  {{ HTML::style('public/bower_components/bootstrap/dist/css/bootstrap.min.css') }}
</head>
<body>
  @yield('content')
</body>
</html>
