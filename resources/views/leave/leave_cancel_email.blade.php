<p>Hi {{$data['name']}},</p>

<p>Your leave application has been cancelled</p>
<h3>Please observe below detail</h3>
@if($data['from_date'] == $data['to_date'])
  <p>You have applied for leave on {{ getDateTime($data['from_date'],'d F Y') }}</p>
@else
  <p>You have applied for leave from:  {{ getDateTime($data['from_date'],'d F Y') }}  to: {{ getDateTime($data['to_date'],'d F Y') }}</p>
@endif
<p>Reason: {{ $data['reason'] }}</p>
