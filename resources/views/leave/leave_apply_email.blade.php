<p>Hi Team,</p>

@if($data['from_date'] == $data['to_date'])
  <p>{{ $data['name'] }} has applied for leave on {{ getDateTime($data['from_date'],'d F Y') }}</p>
@else
  <p>{{ $data['name'] }} has applied for leave from:  {{ getDateTime($data['from_date'],'d F Y') }}  to: {{ getDateTime($data['to_date'],'d F Y') }}</p>
@endif
<p>Reason: {{ $data['reason'] }}</p>

<p>Thanks</p>
<p>Delimp Technology</p>
