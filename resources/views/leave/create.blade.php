@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Leave</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
            <h3 class="box-title pull-right">Leave Balance: {{ $employee->leave_balance }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @include('flash::message')
    <form method="post" action="{{ route('leave.store') }}" enctype="multipart/form-data" class="form">
    @csrf
    <div class="form-group col-sm-6">
    <label for="Date_of_Birth">From Date</label>
    <input class="form-control" name="from_date" value="{{ old('from_date') }}" type="text" id="startdate" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="From Date" autocomplete="off">
    </div>
    <div class="form-group col-sm-6">
    <label for="Joining_Date">To Date</label>
    <input class="form-control" name="to_date"  value="{{ old('to_date') }}" type="text" id="enddate" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="To Date"  autocomplete="off">
    </div>
    <div class="leaveContainer">

    </div>
    <div class="form-group col-sm-12">
    <label for="Reason">Reason</label>
    <input class="form-control" name="reason" value="{{ old('reason') }}" type="text" id="Reason" placeholder="Reason">
    </div>
    <div class="form-group col-sm-12 text-center">
    <input class="btn btn-primary" name="add" type="submit" value="Submit">
    </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  <!-- Leave page -->
  $("#startdate").datepicker({
      todayBtn:  1,
      autoclose: true,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#enddate').datepicker('setStartDate', minDate);
    var enddate= $("#enddate").val();
    if($.trim($("#enddate").val())!=""){
      getDateRange($("#startdate").val(), $("#enddate").val());
    }
  });

  $("#enddate").datepicker({autoclose: true})
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#startdate').datepicker('setEndDate', maxDate);
    getDateRange($("#startdate").val(), $("#enddate").val());
  });


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
  var getDateRange = function(startDate, endDate){
    $.ajax({
      url:'{{ url('leave/getDateRange')}}',
      method:'post',
      data:{startDate:startDate,endDate:endDate},
      dataType:'json',
      beforeSend: function(){
        $(".leaveContainer").html('');
        $('form input[name=add]').attr('disabled','disabled');
      },
      success: function(json){
        if(json.string){
          $(".leaveContainer").html(json.string);
          $('form input[name=add]').removeAttr('disabled');
        }
        else{
          $('form input[name=add]').attr('disabled','disabled');
        }
      }
    })
  }

  var startdateValue = $("#startdate").val();
  var enddateValue = $("#enddate").val();
  if(startdateValue!="" && enddateValue!=""){
    getDateRange(startdateValue, enddateValue);
  }

  $('body').on('change','.leave_type', function(){
    var ele = $(this);
    if(ele.val() == 'half'){
      ele.parent().next().find('select').removeAttr('disabled');
    }
    else{
      ele.parent().next().find('select').attr('disabled','disabled');
    }
  })
</script>
@stop
