@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Leave</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div align="right">

      </div>
      @include('flash::message')
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>Employee</th>
            <th>From</th>
            <th>To</th>
            <th>Apply Date</th>
            <th>Leave  Balance</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$leaves->isEmpty())
            @foreach($leaves as $key=>$leave)
            <tr>
              <td scope="row">{{ $key+1 }}</td>
              <td>{{ $leave->name }}</td>
              <td>{{ date('d F Y', strtotime($leave->from_date)) }}</td>
              <td>{{ date('d F Y', strtotime($leave->to_date)) }}</td>
              <td>{{ date('d F Y', strtotime($leave->created_at)) }}</td>
              <td>{{ $leave->have_leave_balance }}</td>
              <td>{{ $leave->reason }}</td>
              @if($leave->leave_status == 0)
                <td class="text-warning"><strong>Pending</strong></td>
              @elseif($leave->leave_status == 1)
                <td class="text-success"><strong>Approved</strong></td>
              @else
                <td class="text-danger"><strong>Canceled</strong></td>
              @endif
              <td>
                <a class="btn btn-warning" href="{{ route('leave.detail',$leave->id) }}">View</a>
                <form style="display:inline-block;" action="{{ url('leave/destroy', $leave->id) }}" method="post">
                  @csrf
                  @method('GET')
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
              </td>
            </tr>
            @endforeach
          @else
            <tr role="row">
              <td colspan="8" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>

 $('#Date_of_Birth, #Joining_Date, #Anniversary_Date').datepicker({
      autoclose: true
  });
  $(".plus").click(function(){
    $(".documents-container").append($(".document-child:first").clone(true));
  });
  $(".minus").click(function(){
    if($(this).parent().parent().index() > 0){
      $(this).parent().parent().remove();
    }
  });

  <!-- Leave page -->
  $("#startdate").datepicker({
      todayBtn:  1,
      autoclose: true,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#enddate').datepicker('setStartDate', minDate);
  });

  $("#enddate").datepicker({autoclose: true})
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#startdate').datepicker('setEndDate', maxDate);
    getDateRange($("#startdate").val(), $("#enddate").val());
  });


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
  var getDateRange = function(startDate, endDate){
    $.ajax({
      url:'{{ url('leave/getDateRange')}}',
      method:'post',
      data:{startDate:startDate,endDate:endDate},
      dataType:'json',
      beforeSend: function(){
        console.log("+++++Requesting+++++");
      },
      success: function(json){
        if(json.string){
          $(".leaveContainer").html(json.string);
        }
      }
    })
  }

  $('body').on('change','.leave_type', function(){
    var ele = $(this);
    if(ele.val() == 'half'){
      ele.parent().next().find('select').removeAttr('disabled');
    }
    else{
      ele.parent().next().find('select').attr('disabled','disabled');
    }
  })
</script>
@stop
