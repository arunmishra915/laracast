
@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Leave</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
            <h3 class="box-title pull-right">Leave Balance: {{ $employee->leave_balance }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div align="right">
      <a href="{{ url('leave') }}" class="btn btn-success">Back</a>
    </div>
    @include('flash::message')
    <form method="post" action="{{ route('leave.update') }}" enctype="multipart/form-data" class="form">
    @csrf
    <div class="form-group col-sm-6">
    <label for="Date_of_Birth">From Date</label>
    <input class="form-control" name="from_date" value="{{ $leave->from_date }}" type="text" id="startdate" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="From Date" autocomplete="off">
    </div>
    <div class="form-group col-sm-6">
    <label for="Joining_Date">To Date</label>
    <input class="form-control" name="to_date" value="{{ $leave->to_date }}" type="text" id="enddate" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="To Date"  autocomplete="off">
    </div>
    <div class="leaveContainer">
      @if(!empty($leave->leave_detail))
        @foreach($leave->leave_detail as $key=>$detail)
          <div class="form-group col-sm-4">
            <label for="Documents_Available">Date &nbsp;&nbsp;@if($leave->leave_status > 0) @if($detail->is_approved == 1) <strong class="text-success">Approved</strong> @else <strong  class="text-danger">Unapproved</strong> @endif @endif</label>
            <input class="form-control" name="date[{{$key}}]" type="text" value="{{ $detail->date }}" readonly>
          </div>
          <div class="form-group col-sm-4">
            <label for="Documents_Available">Leave Type</label>
            <select class="form-control leave_type" name="leave_type[{{$key}}]">
            <option value="full" @if($detail->leave_type == 'full') selected @endif>Full Day</option>
            <option value="half" @if($detail->leave_type == 'half') selected @endif>Half Day</option>
            </select>
          </div>
          <div class="form-group col-sm-4">
            <label for="Documents_Returned">Half Day For</label>
            <select class="form-control half_type" name="half_type[{{$key}}]" @if($detail->leave_type == 'full') disabled @endif>
            <option value="first half" @if($detail->half_period == 'first half') selected @endif>First Half</option>
            <option value="second half" @if($detail->half_period == 'second half') selected @endif >Second Half</option>
            </select>
          </div>
        @endforeach
      @endif
    </div>
    <div class="form-group col-sm-12">
    <label for="Salary">Reason</label>
    <input class="form-control" name="reason" value="{{ $leave->reason }}" type="text" id="Salary" placeholder="Reason">
    </div>
    @if($leave->leave_status == 0)
    <div class="form-group col-sm-12 text-center">
    <input type="hidden" name="id" value="{{ $leave->id }}">
    <input type="hidden" name="leave_count" value="{{ $leave->leave_count }}">
    <input class="btn btn-primary" name="update" type="submit" value="Update">
    </div>
    @endif
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  <!-- Leave page -->
  $("#startdate").datepicker({
      todayBtn:  1,
      autoclose: true,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#enddate').datepicker('setStartDate', minDate);
    var enddate= $("#enddate").val();
    if($.trim($("#enddate").val())!=""){
      getDateRange($("#startdate").val(), $("#enddate").val());
    }
  });

  $("#enddate").datepicker({autoclose: true})
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#startdate').datepicker('setEndDate', maxDate);
    getDateRange($("#startdate").val(), $("#enddate").val());
  });


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
  var getDateRange = function(startDate, endDate){
    $.ajax({
      url:'{{ url('leave/getDateRange')}}',
      method:'post',
      data:{startDate:startDate,endDate:endDate},
      dataType:'json',
      beforeSend: function(){
        $(".leaveContainer").html('');
        $('form input[name=update]').attr('disabled','disabled');
      },
      success: function(json){
        if(json.string){
          $(".leaveContainer").html(json.string);
          $('form input[name=update]').removeAttr('disabled');
        }
        else{
          $('form input[name=update]').attr('disabled','disabled');
        }
      }
    })
  }

  var startdateValue = $("#startdate").val();
  var enddateValue = $("#enddate").val();
  if(startdateValue!="" && enddateValue!=""){
    //getDateRange(startdateValue, enddateValue);
  }

  $('body').on('change','.leave_type', function(){
    var ele = $(this);
    if(ele.val() == 'half'){
      ele.parent().next().find('select').removeAttr('disabled');
    }
    else{
      ele.parent().next().find('select').attr('disabled','disabled');
    }
  })
</script>
@stop
