<p>Hi {{$data['name']}},</p>

<p>Your leave application has been approved</p>
<h3>Please observe below detail</h3>
@if($data['from_date'] == $data['to_date'])
  <p>You have applied for leave on {{ getDateTime($data['from_date'],'d F Y') }}</p>
@else
  <p>You have applied for leave from:  {{ getDateTime($data['from_date'],'d F Y') }}  to: {{ getDateTime($data['to_date'],'d F Y') }}</p>
@endif
<p>Reason: {{ $data['reason'] }}</p>
<table border="1">
  <tr>
    <th>Date</th>
    <th>Leave Type</th>
    <th>Paid /Unpaid</th>
    <th>Status</th>
  </tr>
  @if(!empty($data['leave_detail']))
    @foreach($data['leave_detail'] as $key=>$detail)
      <tr>
        <th>{{$detail->date}}</th>
        <td>@if($detail->leave_type == 'full') Full Day  @else Half Day @endif</td>
        <td>@if(isset($detail->is_paid_leave) && $detail->is_paid_leave == 1) Paid @else Unpaid @endif</td>
        <td>@if($detail->is_approved == 1) Approved  @else Unapproved @endif</td>
      </tr>
    @endforeach
  @endif
</table>

<p>Thanks</p>
<p>Delimp Technology</p>
