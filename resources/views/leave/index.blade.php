@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Leave</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
            <h3 class="box-title pull-right">Leave Balance: {{ $employee->leave_balance }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <div align="right">
        
      </div>
      @include('flash::message')
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>From</th>
            <th>To</th>
            <th>Apply Date</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$leaves->isEmpty())
            @foreach($leaves as $key=>$leave)
            <tr>
              <td scope="row">{{ $key+1 }}</td>
              <td>{{ date('d F Y', strtotime($leave->from_date)) }}</td>
              <td>{{ date('d F Y', strtotime($leave->to_date)) }}</td>
              <td>{{ date('d F Y', strtotime($leave->created_at)) }}</td>
              <td>{{ $leave->reason }}</td>
              @if($leave->leave_status == 0)
                <td class="text-warning"><strong>Pending</strong></td>
              @elseif($leave->leave_status == 1)
                <td class="text-success"><strong>Approved</strong></td>
              @else
                <td class="text-danger"><strong>Canceled</strong></td>
              @endif
              <td>
                <a class="btn btn-warning" href="{{ url('leave/edit',$leave->id) }}">Edit</a>
                <form style="display:inline-block;" action="{{ url('leave/destroy', $leave->id) }}" method="post">
                  @csrf
                  @method('GET')
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
              </td>
            </tr>
            @endforeach
          @else
            <tr role="row">
              <td colspan="8" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
