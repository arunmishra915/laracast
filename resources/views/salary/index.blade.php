@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Salary</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @include('flash::message')
          <form method="post" action="{{ route('salary.detail') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
          <div class="form-group col-sm-6">
            <select class="form-control" name="year" id="year" required>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
            </select>
          </div>
          <div class="form-group col-sm-6">
            <select class="form-control" name="month" id="month" required>
              <option value="">Select Month</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
          </div>
          <div class="form-group col-sm-12 text-center">
          @if(!empty($employee_id))
            <input type="hidden" name="employee_id" value="{{$employee_id}}">
          @endif
          <input class="btn btn-primary" name="add" type="submit" value="Submit">
          </div>
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body box-profile">

            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
</script>
@stop
