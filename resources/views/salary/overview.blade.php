@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Salary</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @include('flash::message')
          @if($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @endif
          <div align="right">
            <a href="{{ route('employee.index') }}" class="btn btn-success">Back</a>
          </div>
          <table class="table table-bordered table-hover mb-10" role="grid">
            <tr>
              <th>Employee Id</th><th>Name</th><th>Email</th><th>Designation</th>
            </tr>
            <tr>
              <td>{{ $employee->auth_id }}</td><td>{{ $employee->name }}</td><td>{{ $employee->email }}</td><td>{{ $employee->designation }}</td>
            </tr>
            <tr>
              <th>Basic</th><th>HRA</th><th>Conveyance</th><th>Medical Allowance</th>
            </tr>
            <tr>
              <td>{{ number_format($salary['basic'],2) }}</td><td>{{ number_format($salary['hra'],2) }}</td><td>{{ number_format($salary['conveyance'],2) }}</td><td>{{ number_format($salary['medical_allowance'],2) }}</td>
            </tr>
            <tr>
              <th>Total</th><td colspan="3">{{ number_format($salary['total'],2) }}</td>
            </tr>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body box-profile">
              <form method="post" action="{{ route('salary.generatesalary') }}" enctype="multipart/form-data" class="form">
                @csrf
                @method('POST')
              <div class="form-group col-sm-6">
                <select class="form-control" name="year" id="year" onchange="getLeaveInfo(this)" required>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
                <select class="form-control" name="month" id="month" onchange="getLeaveInfo(this)" required>
                  <option value="">Select Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
                </select>
              </div>
              <div class="form-group col-sm-6">
              <label for="Paid_Leaves">Paid Leaves</label>
              <input class="form-control" value="{{ 0 }}" name="paid_leaves" type="text" id="Paid_Leaves" placeholder="Paid Leaves">
              </div>
              <div class="form-group col-sm-6">
              <label for="Unpaid_Leaves">Unpaid Leaves</label>
              <input class="form-control" name="unpaid_leaves" value="{{ 0 }}" type="text" id="Unpaid_Leaves" placeholder="Unpaid Leaves">
              </div>
              <div class="form-group col-sm-6">
              <label for="Bonus_Amount">Bonus Amount</label>
              <input class="form-control" name="bonus_amount" value="{{ 0 }}" type="number" id="Bonus_Amount" placeholder="Bonus Amount">
              </div>
              <div class="form-group col-sm-12 text-center">
              <input type="hidden" name="employee_id" value="{{ $employee->id }}">
              <input class="btn btn-primary" name="add" type="submit" value="Create Salary">
              </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
  var getLeaveInfo = function(value){
    var data = {};
    var selectType = value.name;
    switch(selectType){
      case 'month':{
        data['month'] = value.value;
        data['year'] =  $(value).parents().prev().find('select').val();
      } break;
      case 'year':{
        data['month'] = $(value).parents().next().find('select').val();
        data['year'] =  value.value;
      } break;
      default:{
        alert("Invalid Request");
      }
    }
    if($.trim(data.month)!=""){
      data['employee_id'] = $('input[name=employee_id]').val();
      $.ajax({
        url:'{{ route("leave.info")}}',
        method:'post',
        data:data,
        dataType:'json',
        beforeSend: function(){
          console.log("+++++Requesting+++++");
        },
        success: function(json){
          $("input[name=unpaid_leaves]").val(json.unpaidLeave);
          $("input[name=paid_leaves]").val(json.paidLeave);
        }
      })
    }
  }
</script>
@stop
