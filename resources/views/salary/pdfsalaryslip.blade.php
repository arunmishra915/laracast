@extends('layouts.pdf')
@section('content')
<section class="content" style="padding:0;">
  <div class="row">
    <div class="col-xs-12">
      <div class="box" style="border-top:none;">
          <div class="box-body">
            <table class="table table-hover" role="grid">
              <tr>
                <td style="border:none;"><img src="{{ url('public/dist/img/logo.png') }}" style="width:200px;" /></td>
              </tr>
              <tr>
                <td><strong>A-15, First Floor<br/>
                  Sector-3, Noida<br/>
                  Uttar Pradesh (201301) India</strong>
                </td>
              </tr>
            </table>
            <table class="table table-hover mb-10" role="grid">
              <tr>
                <td style="font-size:24px; text-align:center; font-weight:600; border:none">Pay Slip</td>
              </tr>
            </table>
            <table class="table table-bordered table-hover mb-10" role="grid">
              <tr>
                <th>Name</th><td>{{ $employee->name }}</td>
                <th>Employee ID</th><td>{{ $employee->auth_id }}</td>
              </tr>
              <tr>
                <th>Date of Joining</th><td>{{ getDateTime($employee->joining_date, 'd M Y') }}</td>
                <th>Designation</th><td>{{ $employee->designation }}</td>
              </tr>
              <tr>
                <th>Email Address</th><td>{{ $employee->email }}</td>
                <th>Contact No.</th><td>{{ $employee->contact_number }}</td>
              </tr>
              <tr>
                <th>Bank Account No.</th><td>{{ $employee->bank_account }}</td>
                <th>Pan Card No.</th><td>{{ $employee->pancard }}</td>
              </tr>
              <tr>
                <th>Date From</th><td>{{ getDateTime($userSalary->date_from,'d F Y') }}</td>
                <th>Date To</th><td>{{ getDateTime($userSalary->date_to,'d F Y') }}</td>
              </tr>
              <tr>
                <th>Paid Leaves</th><td>{{ $userSalary->paid_leaves }}</td>
                <th>Unpaid Leaves</th><td>{{ $userSalary->unpaid_leaves }}</td>
              </tr>
              <tr>
                <th>Working Days</th><td>{{ $userSalary->working_days }}</td>
                <th>Salary Per Month</th><td>{{ $userSalary->salary_per_month }}</td>
              </tr>
            </table>
            <table class="table table-bordered table-hover mb-10" role="grid" style="margin-top:20px;">
              <tr>
                <th colspan="2" style="text-align:center">Earning</th><th colspan="2" style="text-align:center">Deduction</th>
              </tr>
              <tr>
                <th>Name</th><th>Total</th><th>Name</th><th>Total</th>
              </tr>
              <tr>
                <td>Basic</td><td>{{ number_format($userSalary->basic,2) }}</td><td>Provident Fund</td><td>{{ number_format($userSalary->provident_fund,2) }}</td>
              </tr>
              <tr>
                <td>HRA</td><td>{{ number_format($userSalary->hra,1) }}</td><td>Professional Tax</td><td>{{ number_format($userSalary->professional_tax,2) }}</td>
              </tr>
              <tr>
                <td>Conveyance</td><td>{{ number_format($userSalary->conveyance,2) }}</td><td>Unpaid Leaves</td><td>{{ number_format($userSalary->unpaid_leaves_amount,2) }}</td>
              </tr>
              <tr>
                <td>Medical Allowance</td><td>{{ number_format($userSalary->medical_allowance,2) }}</td><td></td><td></td>
              </tr>
              <tr>
                <td>Bonus</td><td>{{ number_format($userSalary->bonus_amount,2) }}</td><td></td><td></td>
              </tr>
              <tr>
                <td></td><td></td><td></td><td></td>
              </tr>
              <tr>
                <th>Total Earnings</th><th>{{ number_format($userSalary->total_earning,2) }}</th><th>Total Deductions</th><th>{{ number_format($userSalary->total_deduction,2) }}</th>
              </tr>
              <tr>
                <th></th><th></th><th>Net Salary Payable</th><th>{{ 'Rs. '.number_format($userSalary->net_salary,2) }}</th>
              </tr>
            </table>
            <table class="table table-hover mb-10" role="grid" style="margin-top:10px;">
              <tr>
                <td style="border:none;"><strong><u>Net Salary Payable (In Words) :</u></strong> {{ ucwords($amountInWords) }} Rupees Only.</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
  </div>
</section>
@stop
