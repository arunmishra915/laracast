@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Salary</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @include('flash::message')
          <form method="post" action="{{ route('salary.generatesalary') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
            <div class="form-group col-sm-4">
              <select class="form-control" name="employee_id" id="employee_id" required>
                <option value="">Select Employee</option>
                @if(!$employees->isEmpty())
                  @foreach($employees as $employee)
                    <option {{ old('employee_id') == $employee->id ? 'selected' : '' }} value="{{ $employee->id }}">{{ $employee->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          <div class="form-group col-sm-4">
            <select class="form-control" name="year" id="year" onchange="getLeaveInfo(this)" required>
              <option value="2019" {{ old('year') == '2019' ? 'selected' : '' }}>2019</option>
              <option value="2020" {{ old('year') == '2020' ? 'selected' : '' }}>2020</option>
            </select>
          </div>
          <div class="form-group col-sm-4">
            <select class="form-control" name="month" id="month" onchange="getLeaveInfo(this)" required>
              <option value="">Select Month</option>
              <option value="01" {{ old('month') == '01' ? 'selected' : '' }}>January</option>
              <option value="02" {{ old('month') == '02' ? 'selected' : '' }}>February</option>
              <option value="03" {{ old('month') == '03' ? 'selected' : '' }}>March</option>
              <option value="04" {{ old('month') == '04' ? 'selected' : '' }}>April</option>
              <option value="05" {{ old('month') == '05' ? 'selected' : '' }}>May</option>
              <option value="06" {{ old('month') == '06' ? 'selected' : '' }}>June</option>
              <option value="07" {{ old('month') == '07' ? 'selected' : '' }}>July</option>
              <option value="08" {{ old('month') == '08' ? 'selected' : '' }}>August</option>
              <option value="09" {{ old('month') == '09' ? 'selected' : '' }}>September</option>
              <option value="10" {{ old('month') == '10' ? 'selected' : '' }}>October</option>
              <option value="11" {{ old('month') == '11' ? 'selected' : '' }}>November</option>
              <option value="12" {{ old('month') == '12' ? 'selected' : '' }}>December</option>
            </select>
          </div>
          <div class="form-group col-sm-6">
          <label for="Paid_Leaves">Paid Leaves</label>
          <input class="form-control" value="{{ 0 }}" name="paid_leaves" type="text" id="Paid_Leaves" placeholder="Paid Leaves">
          </div>
          <div class="form-group col-sm-6">
          <label for="Unpaid_Leaves">Unpaid Leaves</label>
          <input class="form-control" name="unpaid_leaves" value="{{ 0 }}" type="text" id="Unpaid_Leaves" placeholder="Unpaid Leaves">
          </div>
          <div class="form-group col-sm-6">
          <label for="Bonus_Amount">Bonus Amount</label>
          <input class="form-control" name="bonus_amount" value="{{ 0 }}" type="number" id="Bonus_Amount" placeholder="Bonus Amount">
          </div>
          <div class="form-group col-sm-6">
          <label for="Working_Days">Working Days</label>
          <input class="form-control" name="working_days" value="{{ 0 }}" type="number" id="Working_Days" placeholder="Working Days">
          </div>
          <div class="form-group col-sm-12 text-center">
          <input class="btn btn-primary" name="add" type="submit" value="Create Salary">
          </div>
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
  var getLeaveInfo = function(value){
    var data = {};
    var selectType = value.name;
    switch(selectType){
      case 'month':{
        data['month'] = value.value;
        data['year'] =  $(value).parents().prev().find('select').val();
      } break;
      case 'year':{
        data['month'] = $(value).parents().next().find('select').val();
        data['year'] =  value.value;
      } break;
      default:{
        alert("Invalid Request");
      }
    }
    if($.trim(data.month)!=""){
      data['employee_id'] = $('#employee_id').val();
      $.ajax({
        url:'{{ route("leave.info")}}',
        method:'post',
        data:data,
        dataType:'json',
        beforeSend: function(){
          console.log("+++++Requesting+++++");
        },
        success: function(json){
          $("input[name=unpaid_leaves]").val(json.unpaidLeave);
          $("input[name=paid_leaves]").val(json.paidLeave);
        }
      })
    }
  }
</script>
@stop
