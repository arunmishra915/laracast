@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Salary</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    @if($userSalary)
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if(in_array($user->role,array(1,2)))
            <div align="right">
              <a href="{{ url('employee') }}" class="btn btn-info">Manage Employee</a>
              <a href="{{ route('salary.create') }}" class="btn btn-success">Back</a>
            </div>
          @endif
          <table class="table table-bordered table-hover mb-10" role="grid">
            <tr>
              <th>Name</th><td>{{ $employee->name }}</td>
              <th>Employee ID</th><td>{{ $employee->auth_id }}</td>
            </tr>
            <tr>
              <th>Date of Joining</th><td>{{ getDateTime($employee->joining_date, 'd M Y') }}</td>
              <th>Designation</th><td>{{ $employee->designation }}</td>
            </tr>
            <tr>
              <th>Email Address</th><td>{{ $employee->email }}</td>
              <th>Contact No.</th><td>{{ $employee->contact_number }}</td>
            </tr>
            <tr>
              <th>Bank Account No.</th><td>{{ $employee->bank_account }}</td>
              <th>Pan Card No.</th><td>{{ $employee->pancard }}</td>
            </tr>
            <tr>
              <th>Date From</th><td>{{ getDateTime($userSalary->date_from,'d F Y') }}</td>
              <th>Date To</th><td>{{ getDateTime($userSalary->date_to,'d F Y') }}</td>
            </tr>
            <tr>
              <th>Paid Leaves</th><td>{{ $userSalary->paid_leaves }}</td>
              <th>Unpaid Leaves</th><td>{{ $userSalary->unpaid_leaves }}</td>
            </tr>
            <tr>
              <th>Working Days</th><td>{{ $userSalary->working_days }}</td>
              <th>Salary Per Month</th><td>{{ $userSalary->salary_per_month }}</td>
            </tr>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body box-profile">
              <table class="table table-bordered table-hover mb-10" role="grid">
                <tr>
                  <th colspan="2">Earning</th><th colspan="2">Deduction</th>
                </tr>
                <tr>
                  <th>Name</th><th>Total</th><th>Name</th><th>Total</th>
                </tr>
                <tr>
                  <td>Basic</td><td>{{ number_format($userSalary->basic,2) }}</td><td>Provident Fund</td><td>{{ number_format($userSalary->provident_fund,2) }}</td>
                </tr>
                <tr>
                  <td>HRA</td><td>{{ number_format($userSalary->hra,1) }}</td><td>Professional Tax</td><td>{{ number_format($userSalary->professional_tax,2) }}</td>
                </tr>
                <tr>
                  <td>Conveyance</td><td>{{ number_format($userSalary->conveyance,2) }}</td><td>Unpaid Leaves</td><td>{{ number_format($userSalary->unpaid_leaves_amount,2) }}</td>
                </tr>
                <tr>
                  <td>Medical Allowance</td><td>{{ number_format($userSalary->medical_allowance,2) }}</td><td></td><td></td>
                </tr>
                <tr>
                  <td>Bonus</td><td>{{ number_format($userSalary->bonus_amount,2) }}</td><td></td><td></td>
                </tr>
                <tr>
                  <td></td><td></td><td></td><td></td>
                </tr>
                <tr>
                  <th>Total Earnings</th><th>{{ number_format($userSalary->total_earning,2) }}</th><th>Total Deductions</th><th>{{ number_format($userSalary->total_deduction,2) }}</th>
                </tr>
                <tr>
                  <th></th><th></th><th>Net Salary Payable</th><th>{{ 'Rs. '.number_format($userSalary->net_salary,2) }}</th>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body box-profile">
              <a href="{{ route('salary.downloadslip',$userSalary->id) }}" class="btn btn-primary">Download Slip</a>
              @if(in_array($user->role, array(1,2)))
              <a href="{{ route('salary.destroy',$userSalary->id) }}" class="btn btn-danger">Delete Slip</a>
              @endif
            </div>
          </div>
      </div>
    </div>
    @else
      <div class="alert alert-warning">
        <strong>Sorry, </strong> there is no data avaliable!
      </div>
    @endif
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('input[name="_token"]').val()
    }
  });
</script>
@stop
