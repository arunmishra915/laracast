@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Holiday</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @if($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <div align="right">
        <a href="{{ route('employee.index') }}" class="btn btn-success">Back</a>
      </div>
      <form method="post" action="{{ route('payroll.store') }}" enctype="multipart/form-data" class="form">
        @csrf
        <div class="form-group col-sm-3">
          <input required class="form-control" name="salary" type="text" placeholder="Salary Amount">
          @error('increased_amount')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="form-group col-sm-3">
          <input required class="form-control" name="increased_amount" value="0" type="text" placeholder="Increased Amount">
          @error('increased_amount')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="form-group col-sm-3">
          <input required id="cal-0" class="form-control holidayDate" name="date" type="text" data-date-format="yyyy-mm-d" placeholder="Select Date" autocomplete="off">
          @error('date')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="form-group col-sm-3 text-center">
          <input type="hidden" name="user_id" value="{{ $employee->id }}">
        <input class="btn btn-primary" name="add" type="submit" value="Submit">
      </div>
    </form>
    </div>
          <!-- /.box-body -->
        </div>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th>
            <th>Date</th>
            <th>Salary</th>
            <th>Increased Amount</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!$payrolls->isEmpty())
           @foreach($payrolls as $key=>$payroll)
           <tr>
             <td scope="row">{{ $key+1 }}</td>
             <td>{{ date('d F Y', strtotime($payroll->date)) }}</td>
             <td>{{ number_format($payroll->salary,2) }}</td>
             <td>{{ number_format($payroll->increased_amount,2) }}</td>
             <td>
               <form style="display:inline-block;" action="{{ route('payroll.destroy', $payroll->id) }}" method="post">
                 @csrf
                 @method('DELETE')
                 <button type="submit" class="btn btn-danger">Delete</button>
               </form>
             </td>
           </tr>
           @endforeach
          @else
            <tr role="row">
              <td colspan="5" class="alert alert-warning"><strong>Sorry</strong> , there is no data to display</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script type="text/javascript">
  $("#cal-0").datepicker({autoclose: true,orientation:"down"});
</script>
@stop
