@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>Dashboard</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Tables</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Report</h3>
          </div>
          <!-- /.box-header -->
    <div class="box-body">
      @include('flash::message')
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Sn</th><th>Name</th><th>Email</th><th>Message</th><th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($contacts as $key=>$contact)
          <tr>
            <td scope="row">{{ $key+1 }}</td><td>{{$contact['name']}}</td><td>{{$contact['email']}}</td><td>{{$contact['msg']}}</td>
            <td>
              <a class="text-success" href="{{url('contact/view/'.$contact['id'])}}">View</a>&nbsp;|
              <a class="text-warning" href="{{url('contact/edit/'.$contact['id'])}}">Edit</a>&nbsp;|
              <a class="text-danger" href="{{url('contact/delete/'.$contact['id'])}}">Delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
