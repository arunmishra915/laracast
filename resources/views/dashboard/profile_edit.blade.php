@extends('layouts.default')
@section('content')
  <section class="content-header">
    <h1>
      Dashboard
      <small>{{ $pageHeading }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Employee</a></li>
      <li class="active">{{ $pageHeading }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ $pageHeading }}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @include('flash::message')
          <div align="right">
            <a href="{{ url('dashboard') }}" class="btn btn-success">Back</a>
          </div>
          <form method="post" action="{{ route('dashboard.updateprofile') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
          <div class="form-group col-sm-6">
          <label for="Name">Employee Name</label>
          <input class="form-control" value="{{ $employee->name }}" name="name" type="text" id="Name" placeholder="Employee Name" disabled>
          </div>
          <div class="form-group col-sm-6">
          <label for="Email">Employee Email</label>
          <input class="form-control" name="email" value="{{ $employee->email }}" type="text" id="Email" placeholder="Employee Email" disabled>
          </div>
          <div class="form-group col-sm-6">
          <label for="Auth_Id">Password</label>
          <input class="form-control" name="password" type="text" id="Password" placeholder="Password">
          </div>
          <div class="form-group col-sm-6">
          <label for="Confirm_Password">Password Confirmation</label>
          <input class="form-control" name="password_confirmation" type="text" id="Confirm_Password" placeholder="Confirm Password">
          </div>
          <div class="form-group col-sm-6">
          <label for="Contact_Number">Contact Number</label>
          <input class="form-control" value="{{ $employee->contact_number }}" name="contact_number" type="text" id="Contact_Number" placeholder="Contact Number" disabled>
          </div>
          <div class="form-group col-sm-6">
          <label for="Emergency_Contact_Number">Emergency Contact Number</label>
          <input class="form-control" value="{{ $employee->emergency_contact_number }}" name="emergency_contact_number" type="text" id="Emergency_Contact_Number" placeholder="Emergency Contact Number" disabled>
          </div>
          <div class="form-group col-sm-6">
          <label for="Date_of_Birth">Date of Birth</label>
          <input class="form-control" value="{{ $employee->dob }}" name="dob" type="text" id="Date_of_Birth" class="datepickerE" data-date-format="yyyy-mm-d" placeholder="Date of Birth" autocomplete="off" disabled>
          </div>
          <div class="form-group col-sm-6">
          <label for="image">Image</label>
          <input class="form-control" name="image" type="file" id="image">
          </div>
          @if($employee->image!='')
          <div class="form-group col-sm-6">

          </div>
          <div class="form-group col-sm-6">
          <img src="{{ url('public/profile',$employee->image) }}" style="width:100%;" alt="">
          <input type="hidden" name="old_image" value="{{ $employee->image }}">
          </div>
          @endif
          <div class="form-group col-sm-12 text-center">
          <input class="btn btn-primary" name="add" type="submit" value="Update">
          </div>
          </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop
@section('pagejs')
<script>
 $('#Date_of_Birth, #Joining_Date, #Anniversary_Date').datepicker({
    autoclose: true
  });
</script>
@stop
