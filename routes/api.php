<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


  Route::any('login', 'API\LoginController@processLogin');
  Route::post('userdetail', 'API\UserController@getUserDetail');
  Route::get('getholiday', 'API\UserController@getHoliday');
  Route::get('getpolicydata', 'API\UserController@getPolicyData');
  Route::post('updateprofile', 'API\UserController@updateProfile');
  Route::post('updateuser', 'API\UserController@updateUser');
  Route::post('getsalarydetail', 'API\UserController@getSalaryDetail');
  Route::get('downloadslip/{id}', 'API\UserController@downloadSlip');
  Route::post('forgotpassword', 'API\LoginController@processforgotpass');
  Route::post('getdaterange', 'API\LeaveController@getDateRange');
  Route::post('submitleave', 'API\LeaveController@submitLeave');
  Route::post('myleaves', 'API\LeaveController@myLeaves');
  Route::post('deleteleave', 'API\LeaveController@deleteLeave');
  Route::post('sendfeedback', 'API\UserController@sendFeedback');
//Route::post('login', ['middleware' => 'cors' , 'uses'=> 'API\LoginController@processLogin']);
