<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect()->route('contact.create');
// });
Route::get('/', 'LoginController@index')->name('login.index');
Route::get('login', 'LoginController@index')->name('login.index');
Route::post('login', 'LoginController@processlogin')->name('login.processlogin');
Route::get('logout', 'LoginController@logout');
Route::get('forgotpassword', 'LoginController@forgotpassword')->name('login.forgotpassword');
Route::post('forgotpassword', 'LoginController@processforgotpass')->name('login.processforgotpass');
Route::get('adjustleavebalance', 'LoginController@adjustLeaveBalance');


Route::group(['middleware' => 'checkLogin'], function () {
  Route::resource('employee', 'EmployeeController')->middleware('userAuth');
  Route::get('employee/document/{id}', 'EmployeeController@document')->where(['id'=>'[0-9]+']);
  Route::post('employee/documentupload', 'EmployeeController@documentupload');
  Route::get('employee/document_edit/{id}', 'EmployeeController@document_edit')->where(['id'=>'[0-9]+']);
  Route::post('employee/document_update', 'EmployeeController@document_update');
  Route::get('employee/document_destroy/{id}', 'EmployeeController@document_destroy')->where(['id'=>'[0-9]+']);

  Route::get('dashboard', 'DashboardController@index');
  Route::get('dashboard/profile_edit', 'DashboardController@profile_edit');
  Route::post('dashboard/updateprofile', 'DashboardController@updateprofile')->name('dashboard.updateprofile');

  Route::get('leave/create', 'LeaveController@create')->name('leave.create');
  Route::post('leave/create', 'LeaveController@store')->name('leave.store');
  Route::post('leave/getDateRange', 'LeaveController@getDateRange');
  Route::get('leave', 'LeaveController@index')->name('leave.view');
  Route::get('leave/edit/{id}', 'LeaveController@edit')->where(['id'=>'[0-9]+']);
  Route::post('leave/update', 'LeaveController@update')->name('leave.update');
  Route::get('leave/leaverequests', 'LeaveController@leaverequests')->middleware('userAuth')->name('leave.leaverequests');
  Route::get('leave/detail/{id}', 'LeaveController@detail')->name('leave.detail');
  Route::post('leave/statusupdate', 'LeaveController@statusupdate')->name('leave.statusupdate');
  Route::get('leave/destroy/{id}', 'LeaveController@destroy')->where(['id'=>'[0-9]+'])->name('leave.destroy');
  Route::post('leave/getLeaveInfo', 'LeaveController@getLeaveInfo')->name('leave.info');

  Route::get('policy', 'PolicyController@index')->name('policy.index');
  Route::get('policy/edit', 'PolicyController@edit')->middleware('userAuth')->name('policy.edit');
  Route::post('policy/update', 'PolicyController@update')->name('policy.update');

  Route::resource('holiday', 'HolidayController',['except'=>['index']])->middleware('userAuth');
  Route::get('holiday', 'HolidayController@index')->name('holiday.index');

  Route::get('feedback', 'FeedbackController@index')->name('feedback.index');
  Route::post('feedback', 'FeedbackController@store')->name('feedback.store');
  Route::get('feedback/view', 'FeedbackController@view')->middleware('userAuth')->name('feedback.view');
  Route::get('feedback/getfeedback/{id}', 'FeedbackController@getfeedback')->middleware('userAuth')->name('feedback.getfeedback');
  Route::post('feedback/getPreviousChat', 'FeedbackController@getPreviousChat');

  Route::get('salary','SalaryController@index')->name('salary.index');
  Route::post('salary','SalaryController@getSalaryDetail')->name('salary.detail');
  Route::get('salary/create', 'SalaryController@create')->middleware('userAuth')->name('salary.create');
  Route::get('salary/overview/{id}', 'SalaryController@overview')->middleware('userAuth')->name('salary.overview')->where(['id'=>'[0-9]+']);
  Route::post('salary/generateSalary', 'SalaryController@generateSalary')->name('salary.generatesalary');
  Route::get('salary/downloadSlip/{id}', 'SalaryController@downloadSlip')->name('salary.downloadslip');
  Route::get('salary/destroySlip/{id}', 'SalaryController@destroySlip')->name('salary.destroy');
  Route::get('salary/viewSalary/{id}', 'SalaryController@viewSalary')->name('salary.view');
  Route::resource('payroll', 'PayrollController')->middleware('userAuth');
  Route::get('payroll/create/{id}', 'PayrollController@create')->middleware('userAuth')->name('payroll.create')->where(['id'=>'[0-9]+']);
  // Route::resource('suggestion', 'SuggestionController',['except'=>['create']])->middleware('userAuth');
  // Route::post('suggestion/create', 'SuggestionController@create')->name('suggestion.create');
  Route::resource('suggestion', 'SuggestionController');
});


// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('noida', 'NoidaController@index');
// Route::get('hello', 'hellocontroller@hello')->middleware('test');
// Route::get('sayhello', 'hellocontroller@index');
// Route::get('test/{fname}', 'hellocontroller@test');
// Route::get('demo/{price}', function($price){
//   echo $price;
// })->where(["price"=>"[0-9]+"]);
// Route::get('price/{max}/{min?}', function($max, $min=0){
//   echo 'price is between: Max: '.$max.' and Min: '.$min;
// });
