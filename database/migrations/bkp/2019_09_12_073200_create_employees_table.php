<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      Schema::create('employees', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->string('designation');
        $table->date('joining_date');
        $table->string('contact_number', 20);
        $table->string('emergency_contact_number', 20);
        $table->date('dob');
        $table->date('anniversary_date');
        $table->double('salary', 8, 2);
        $table->enum('document_available', ['yes', 'no']);
        $table->enum('document_returned', ['yes', 'no']);
        $table->enum('status', ['active', 'left']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::dropIfExists('employees');
    }
}
